import os
import sys
from datetime import datetime

from flask.ext.script import Server, Manager, Command
from flask.ext.script.commands import Clean, ShowUrls
from quill.common import settings
from quill.common.models import Document

from quill.common.settings import LOGGING_FORMAT, DATE_FORMAT

dirname = os.path.dirname(os.path.dirname(__file__))
sys.path.insert(0, dirname)


class Analyzer(Command):
    """
    Custom command to run the analyzer from
    the command line
    """

    def run(self):
        import logging
        logging.basicConfig(
            format=LOGGING_FORMAT,
            datefmt=DATE_FORMAT,
            level=logging.INFO
        )

        start = datetime.now()

        # Override the save and clear behaviors
        settings.TEST_RUNNER = False

        for ii in xrange(5):
            doc = Document()
            doc = doc.from_file(
                'test-user-id', open(
                    settings.DATA_FILE, 'rb'
                )
            )

            logging.info(repr(doc))
        end = datetime.now()

        logging.info("Analysis took: " + str(end - start))


if __name__ == "__main__":
    # Setup
    from quill.web import create_app
    app = create_app(debug=True)

    # Create an app manager
    manager = Manager(app)

    manager.add_command("runserver", Server(processes=4))
    manager.add_command("clean", Clean())
    manager.add_command("urls", ShowUrls())

    # Add other commands:
    #   python manage.py analyzer
    manager.add_command("analyzer", Analyzer())

    manager.run()
