from dateutil import parser
from datetime import datetime


def contains_date(sentence):
    try:
        # Look for a date in the sentence
        date = parser.parse(sentence, fuzzy=True)
        if isinstance(date, datetime):
            now = datetime.now()

            # Fuzzy parse returns today if it
            # can't find anything
            if (date.day != now.day and
                date.year != now.year and
                date.month != now.month):
                    return True
            else:
                return False
    except:
        return False