"""
Module for determining if a word is a homophone

http://www.englishclub.com/pronunciation/homophones-list-5.htm
"""

HOMOPHONES = """
air / heir
aisle / isle
ante- / anti-
bare / bear
brake / break
buy / by
cell / sell
cent / scent
cereal / serial
coarse / course
complement / compliment
dam / damn
dear / deer
die / dye
fair / fare
fir / fur
flour / flower
for / four
hair / hare
heal / heel
hear / here
him / hymn
hole / whole
hour / our
idle / idol
knight / night
made / maid
mail / male
meat / meet
morning / mourning
none / nun
one / won
pair / pear
peace / piece
plain / plane
poor / pour
pray / prey
principal / principle
profit / prophet
real / reel
right / write
root / route
sail / sale
sea / see
seam / seem
sight / site
sew / so
shore / sure
sole / soul
some / sum
son / sun
stair / stare
stationary / stationery
steal / steel
suite / sweet
tail / tale
their / there
toe / tow
waist / waste
wait / weight
way / weigh
weak / week
wear / where

bean / been
buy / by
hear / here
hour / our
knows / nose
mail / male
meat / meet
plain / plane
prince / prints
right / write
road / rode
sail / sale
sea / see
son / sun
steal / steel
storey / story
tail / tale
their / there
wear / where
wood / would

ate / eight
blew / blue
brake / break
cell / sell
cent / sent
creak / creek
dear / deer
feat / feet
find / fined
hair / hare
heal / he'll
hole / whole
made / maid
new / knew
one / won
passed / past
poor / pour
shore / sure
sole / soul
stair / stare
their / they're
theirs / there's
threw / through
throne / thrown
way / weigh
we'd / weed
we'll / wheel
which / witch
who's / whose
your / you're

allowed / aloud
band / banned
bare / bear
billed / build
boar / bore
board / bored
bold / bowled
bread / bred
bridal / bridle
cent / scent
cereal / serial
chews / choose
crews / cruise
days / daze
desert / dessert
dies / dyes
fair / fare
farther / father
fir / fur
flew / flu
flour / flower
gnus / news
guessed / guest
guise / guys
hay / hey
heal / heel
heard / herd
higher / hire
I'll / isle
knead / need
knight / night
mind / mined
missed / mist
nun / none
ode / owed
oh / owe
pail / pale
pairs / pears
pause / paws
peace / piece
peer / pier
pole / poll
pore / pour
pray / prey
raise / rays
rapper / wrapper
root / route
rose / rows
saw / sore
sew / sow
sight / site
soared / sword
tease / tees
tense / tents
toad / towed
toes / tows
wait / weight
war / wore
wares / wears
warn / worn
weakly / weekly
weather / whether
weave / we've

altar / alter
bail / bale
bald / bawled
ball / bawl
base / bass
bases / basis
berries / buries
bough / bow
brews / bruise
ceiling / sealing
cellar / seller
chili / chilly
cite / sight
clause / claws
climb / clime
coarse / course
coo / coup
cue / queue
faint / feint
faze / phase
few / phew
file / phial
flair / flare
flaw / floor
foul / fowl
frays / phrase
gel / jell
genes / jeans
gilled / guild
gilt / guilt
grade / greyed
grate / great
graze / greys
groan / grown
heard / herd
heel / he'll
idle / idol
lacks / lax
lays / laze
leased / least
loch / lock
locks / lox
loot / lute
mall / maul
manner / manor
mewl / mule
mews / muse
miner / minor
moat / mote
mode / mowed
muscle / mussel
paced / paste
palate / palette
peak / pique
praise / prays
pride / pried
pries / prize
principal / principle
profit / prophet
sac / sack
sacks / sax
scents / sense
seamen / semen
seas / seize
side / sighed
sighs / size
sign / sine
sink / sync
slay / sleigh
some / sum
straight / strait
suede / swayed
suite / sweet
sundae / Sunday
tacks / tax
Thai / tie
thyme / time
told / tolled
tracked / tract
troop / troupe
trussed / trust
tucks / tux
yoke / yolk

ail / ale
airs / heirs
aisle / I'll
ascent / assent
aural / oral
auricle / oracle
berth / birth
boy / buoy
cached / cashed
carrot / karat
cede / seed
censor / sensor
chased / chaste
choirs / quires
chords / cords
chute / shoot
coax / cokes
cocks / cox
coffer / cougher
colonel / kernel
cops / copse
core / corps
cygnet / signet
cymbal / symbol
dew / due
done / dun
draft / draught
earns / urns
ewes / use
eyelet / islet
gnu / knew
halls / hauls
heed / he'd
hertz / hurts
hoarse / horse
holy / wholly
instance / instants
intense / intents
jewels / joules
key / quay
knap / nap
knead / need
knit / nit
knob / nob
lichens / likens
licker / liquor
lieu / loo
links / lynx
loon / lune
marshal / martial
medal / meddle
metal / mettle
oohs / ooze
racks / wracks
rapt / wrapped
recede / reseed
receipt / reseat
reek / wreak
reign / rein
rest / wrest
review / revue
rex / wrecks
ring / wring
rite / wright
rote / wrote
rude / rued
rye / wry
taught / taut
tear / tier
vail / veil
vain / vein
variance / variants
vial / vile
wade / weighed
watts / what's
wright / write

trial / trail
"""

# Load this dictionary into memory,
# normalize case to lower
HOMOPHONES_DICTIONARY = set()
for line in HOMOPHONES.splitlines():
    split_line = line.lower().split('/')
    for token in split_line:
        HOMOPHONES_DICTIONARY.add(token.strip())
HOMOPHONES_DICTIONARY.remove('')


def is_homophone(word):
    """
    Helper to determine if a word is a homophone
    """

    if word.lower() in HOMOPHONES_DICTIONARY:
        return True
    return False


def contains_homophone(sentence):

    for word in sentence.split(' '):
        if is_homophone(word):
            return True
    return False
