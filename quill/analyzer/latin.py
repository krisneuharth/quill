"""
Module for determining if a word is latin

http://en.wikipedia.org/wiki/List_of_legal_Latin_terms
"""

LATIN = """
a fortiori
a mensa et thoro
a posteriori
a priori
a quo
ab extra
ab initio
absque hoc
actus reus
ad coelum
ad colligenda bona
ad hoc
ad hominem
ad idem
ad infinitum
ad litem
ad quod damnum
ad valorem
adjournment sine die
affidavit
alter ego
amicus curiae
animus nocendi
animus possidendi
animus revertendi
ante
arguendo
Audi alteram partem
bona fide
bona vacantia
Cadit quaestio
Casus belli
casus fortuitus
Caveat
Caveat emptor
Certiorari
Ceteris paribus
cogitationis poenam nemo patitur
compensatio morae
compos mentis
Condicio sine qua non
consensus ad idem
consensus facit legem
consuetudo pro lege servatur
contra
contra bonos mores
contra legem
Contradictio in adjecto
contra proferentem
coram non judice
corpus delicti
corpus juris
corpus juris civilis
corpus juris gentium
corpus juris secundum
crimen falsi
cui bono
cuius est solum eius est usque ad coelum et ad inferos
de bonis asportatis
debellatio
de bonis non administratis
de die in diem
de facto
de futuro
de integro
de jure
de lege ferenda
de lege lata
delegatus non potest delegare
de minimis
de minimis non curat lex
de mortuis nil nisi bonum
de novo
defalcation
deorum injuriae diis curae
dictum
doli incapax
dolus specialis
donatio mortis causa
dramatis personae
dubia in meliorem partem interpretari debent
duces tecum
ei incumbit probatio qui dicit
ejusdem generis
eo nomine
erga omnes
ergo
erratum
et al.
et cetera
et seq.
et uxor
et vir
ex aequo et bono
ex ante
ex cathedra
ex concessis
ex delicto
ex facie
ex gratia
ex injuria jus non oritur
ex officio
ex parte
ex post
ex post facto
ex post facto law
expressio unius est exclusio alterius
ex proprio motu
ex rel
ex turpi causa non oritur actio
exempli gratia
ex tunc
ex nunc
extant
factum
facio ut facias
favor contractus
felo de se
ferae naturae
fiat
Fiat justitia et pereat mundus
fiat justitia ruat caelum
fieri facias
fortis attachiamentum, validior praesumptionem
forum non conveniens
fructus industriales
fumus boni iuris
functus officio
generalia specialibus non derogant
gravamen
guardian ad litem
habeas corpus
hostis humani generis
i.e.
ibid.
idem
ignorantia juris non excusat
imprimatur
in absentia
In articulo mortis
in camera
in curia
in esse
in extenso
in extremis
in flagrante delicto
in forma pauperis
in futuro
in haec verba
in limine
in loco parentis
in mitius
in omnibus
in pari delicto
in pari materia
in personam
in pleno
in prope persona
in propria persona
in re
in rem
in situ
in solidum
in terrorem
in terrorem clause
in toto
indicia
infra
innuendo
inter alia
inter arma enim silent leges
inter rusticos
inter se
inter vivos
intra
intra fauces terra
intra legem
intra vires
ipse dixit
ipsissima verba
ipso facto
iudex non calculat
jura novit curia
jurat
juris et de jure
jus
jus accrescendri
jus ad bellum
jus civile
jus cogens
jus commune
jus gentium
jus in bello
jus inter gentes
jus naturale
jus primae noctis
jus sanguinis
jus soli
jus tertii
lacunae
leges humanae nascuntur, vivunt, moriuntur
lex communis
lex lata
lex loci
lex posterior derogat priori
lex retro non agit
lex scripta
lex specialis derogat legi generali
liberum veto
lingua franca
lis alibi pendens
lis pendens
locus
locus delicti
locus in quo
locus poenitentiae
locus standi
male fide
malum in se
malum prohibitum
mandamus
mare clausum
mare liberum
mens rea
modus operandi
mora accipiendi
mora solvendi
mortis causa
mos pro lege
motion in limine
mutatis mutandis
ne exeat
ne bis in idem
negotorium gestio
nemo auditur propriam turpitudinem allegans
nemo dat quod non habet
nemo debet esse iudex in propria
nemo judex in sua causa
nemo plus iuris ad alium transferre potest quam ipse habet
nihil dicit
nisi
nisi prius
nolle prosequi
nolo contendere
non adimpleti contractus
non compos mentis
non constat
non est factum
non faciat malum, ut inde veniat bonum
non liquet
non obstante verdicto
novus actus interveniens
noscitur a sociis
nota bene
nudum pactum
nulla bona
nulla poena sine lege
nullum crimen, nulla poena sine praevia lege poenali
nunc pro tunc
obiter dictum
pacta sunt servanda
par delictum
parens patriae
pater familias
pendente lite
per capita
per contra
per curiam
per incuriam
per minas
per quod
per se
per stirpes
periculum in mora
persona non grata
posse comitatus
post mortem
post mortem auctoris
praetor peregrinus
prima facie
primogeniture
prius quam exaudias ne iudices
probatio vincit praesumptionem
pro bono
pro bono publico
pro forma
pro hac vice
pro per
pro rata
pro se
pro tanto
pro tem
pro tempore
propria persona
prout patet per recordum
qua
quareitur
quaere
quantum
quantum meruit
quantum valebant
quasi
qui facit per alium facit per se
qui tam
quid pro quo
quo ante
quo warranto
quoad hoc
quod est necessarium est licitum
ratio decidendi
ratio scripta
rationae soli
rebus sic stantibus
reddendo singula singulis
res
res communis
res gestae
res ipsa loquitur
res judicata
res nullius
res publica
res publica christiana
respondeat superior
restitutio in integrum
salus populi suprema lex esto
scandalum magnatum
scienter
scire facias
scire feci
se defendendo
seriatim
sic utere tuo ut alienum non laedas
sine die
sine qua non
situs
solutio indebiti
stare decisis
status quostatus quo antestatu quo
stratum
sua sponte
sub judice
sub modo
sub nomine
sub silentio
subpoena
subpoena ad testificandum
subpoena duces tecum
suggestio falsi
sui generis
sui juris
suo moto
supersedeas
suppressio veri
supra
terra nullius
trial de novo
trinoda necessitas
uberrima fides
ultra posse nemo obligatur
ultra vires
uno flatu
uti possidetis
uxor
vel non
veto
vice versa
vide
videlicet
vinculum juris
vis major
viz.
volenti non fit injuria
vigilantibus non dormientibus aequitas subvenit
"""

# Load this dictionary into memory,
# normalize case to lower
LATIN_DICTIONARY = set()
for line in LATIN.splitlines():
    for word in line.split(' '):
        LATIN_DICTIONARY.add(word.lower())

LATIN_DICTIONARY.remove('')
LATIN_DICTIONARY.remove('in')
LATIN_DICTIONARY.remove('a')


def is_latin(word):
    """
    Helper for a dictionary lookup to
    see if a word is latin or not
    """

    if word.lower() in LATIN_DICTIONARY:
        return True
    return False


def contains_latin(sentence):
    """
    Helper for seeing which Latin phrases
    appear in the document
    """

    for word in sentence.split(' '):
        if is_latin(word):
            return True
    return False
