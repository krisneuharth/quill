"""
Module for determining if a sentence contains
a legalese that could be more simply worded
"""

LEGALESE = """
the means by which / how
entered a contract to / contracted
filed a counterclaim / counterclaimed
filed a motion / moved
filed an application / applied
adequate number of / enough
for the reason that / because
in the event of / if
in light of the fact that / because
notwithstanding the fact that / although
notwithstanding / despite
cause of action / claim
in order to / to
at this point in time / now
until such time as / until
whether or not / whether

by means of / by
as a consequence of / because of

at a later date / later
is of the opinion that / believes
effectuate / cause
in violation of / violates
is violative of / violates
made a complaint / complained
utilize / use

made application / applied
made provision / provided
contended by / contends
with regard to / about
in connection with / with
search on / searched

each and every / either one
provide responses / respond
offer testimony / testify
make inquiry / ask
provide assistance / help
place a limitation upon / limit
make an examination of / examine
provide protection to / protect
reach a resolution / resolve
bears a significant resemblance / resembles
reveal the identity of / identify
makes mention of / mentions
are in compliance with / comply
make allegations / allege
was in conformity with / conformed
to effect settlement / settle
"""

# Load this dictionary into memory,
# normalize case to lower
LEGALESE_DICTIONARY = set()
for line in LEGALESE.splitlines():
    split_line = line.lower().split('/')
    LEGALESE_DICTIONARY.add(split_line[0].strip())
LEGALESE_DICTIONARY.remove('')


def is_legalese(word):
    """
    Helper for a dictionary lookup to
    see if a word is legalese or not
    """

    if word.lower() in LEGALESE_DICTIONARY:
        return True
    return False


def contains_legalese(sentence):
    """
    Helper for seeing if a party appears in
    a sentence
    """

    for term in LEGALESE_DICTIONARY:
        if term in sentence.lower():
            return True
    return False
