
def over_sentence_length(sentence, limit):
    if len(sentence.split(' ')) >= limit:
        return True
    else:
        return False


def over_paragraph_length(document, paragraph, limit):
    # Cheap way to get sentences in this paragraph
    # without parsing the paragraph again
    sentences_in_p = filter(
        lambda s: s.pid == paragraph.pid,
        document.sentences
    )

    if len(sentences_in_p) >= limit:
        return True
    else:
        return False