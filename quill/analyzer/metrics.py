import math

# Good reference:
# http://www.ideosity.com/ourblog/post/ideosphere-blog/2010/01/14/readability-tests-and-formulas#Hull

# TODO: Write more specific tests for each metric


class Metric(object):
    abstract = True

    @staticmethod
    def score(document):
        raise NotImplementedError

    def __repr__(self):
        return self.__class__.__name__


def to_grade_level(years):
    """
    Convert years of schooling to
    approximate grade level
    """

    YEARS_TO_GRADE = {
        '0': 'Baby',
        '1': 'Infant',
        '2': 'Toddler',
        '3': 'Preschool',
        '4': 'Kindergarten',
        '5': 'Kindergarten',
        '6': '1st',
        '7': '2nd',
        '8': '3rd',
        '9': '4th',
        '10': '5th',
        '11': '6th',
        '12': '7th',
        '13': '8th',
        '14': '9th',
        '15': '10th',
        '16': '11th',
        '17': '12th',
        '18': 'College Freshmen',
        '19': 'College Sophomore',
        '20': 'College Junior',
        '21': 'College Senior',
        '22': 'Graduate Level',
        '23': 'Graduate Level',
        '24': 'Graduate Level',
        '25': 'Post-Graduate Level',
    }

    return YEARS_TO_GRADE.get(
        str(years),
        'Post-Graduate Level'
    )


class ARI(Metric):
    """
    Corresponds to appropriate grade level

    http://en.wikipedia.org/wiki/Automated_Readability_Index
    """

    @staticmethod
    def score(document):
        CHAR_COUNT = document.summary.characters
        WORD_COUNT = document.summary.words
        SENTENCE_COUNT = document.summary.sentences

        ari_score = 4.71 * (CHAR_COUNT / WORD_COUNT) + 0.5 * (WORD_COUNT / SENTENCE_COUNT) - 21.43
        return ari_score


class ColemanLiauIndex(Metric):
    """
    Corresponds to appropriate grade level

    http://en.wikipedia.org/wiki/Coleman-Liau_Index
    """

    @staticmethod
    def score(document):
        CHAR_COUNT = document.summary.characters
        WORD_COUNT = document.summary.words
        SENTENCE_COUNT = document.summary.sentences

        cli_score = (5.89 * (CHAR_COUNT / WORD_COUNT)) - (30 * (SENTENCE_COUNT / WORD_COUNT)) - 15.8
        return cli_score


class FleschKincaidGradeLevel(Metric):
    """
    Corresponds to appropriate grade level

    http://en.wikipedia.org/wiki/Flesch-Kincaid_Readability_Test
    """

    @staticmethod
    def score(document):
        AVG_WORDS_PER_SENTENCE = document.summary.avg_words_per_sentence
        SYLLABLE_COUNT = document.summary.syllables
        WORD_COUNT = document.summary.words

        fkg_score = 0.39 * AVG_WORDS_PER_SENTENCE + 11.8 * (SYLLABLE_COUNT / WORD_COUNT) - 15.59
        return fkg_score


class FleschReadingEase(Metric):
    """
    90.0 - 100.0 = easily understood by an average 11-year-old student
    60.0 - 70.0  = easily understood by 13- to 15-year-old students
     0.0 - 30.0  = best understood by university graduates

    http://en.wikipedia.org/wiki/Flesch-Kincaid_Readability_Test
    """

    @staticmethod
    def score(document):
        AVG_WORDS_PER_SENTENCE = document.summary.avg_words_per_sentence
        SYLLABLE_COUNT = document.summary.syllables
        WORD_COUNT = document.summary.words

        fre_score = 206.835 - (1.015 * AVG_WORDS_PER_SENTENCE) - (84.6 * (SYLLABLE_COUNT / WORD_COUNT))
        return fre_score


class GunningFogIndex(Metric):
    """
    Estimates the years of formal education needed to
    understand the text on a first reading

    http://en.wikipedia.org/wiki/Gunning_fog_index
    """

    @staticmethod
    def score(document):
        AVG_WORDS_PER_SENTENCE = document.summary.avg_words_per_sentence
        COMPLEX_WORD_COUNT = document.summary.complex_words
        WORD_COUNT = document.summary.words

        gf_score = 0.4 * ((AVG_WORDS_PER_SENTENCE + (100 * (COMPLEX_WORD_COUNT / WORD_COUNT))))
        return gf_score


class LIX(Metric):
    """
    Score - not grade level
    (favors long words)

    0-24	Very easy
    25-34	Easy
    35-44	Standard
    45-54	Difficult
    55+	    Very difficult
    """

    @staticmethod
    def score(document):
        WORDS = document.words
        WORD_COUNT = document.summary.words
        SENTENCE_COUNT = document.summary.sentences
        LONG_WORD_COUNT = 0.0

        for word in WORDS:
            if len(word.text) >= 7:
                LONG_WORD_COUNT += 1.0

        lix_score = WORD_COUNT / SENTENCE_COUNT + float(100 * LONG_WORD_COUNT) / WORD_COUNT
        return lix_score


class RIX(Metric):
    """
    Simpler version of the LIX
    Grade level estimate
    (favors long words)

    7.2 and above	College
    6.2 and above	12
    5.3 and above	11
    4.5 and above	10
    3.7 and above	9
    3.0 and above	8
    2.4 and above	7
    1.8 and above	6
    1.3 and above	5
    0.8 and above	4
    0.5 and above	3
    0.2 and above	2
    Below 0.2	    1
    """

    @staticmethod
    def score(document):
        WORDS = document.words
        SENTENCE_COUNT = document.summary.sentences
        LONG_WORD_COUNT = 0.0

        for word in WORDS:
            if len(word.text) >= 7:
                LONG_WORD_COUNT += 1.0

        rix_score = LONG_WORD_COUNT / SENTENCE_COUNT
        return rix_score


class McAlpineEFLAW(Metric):
    """
     1-20	very easy to understand
    21-25	quite easy to understand
    26-29	a little difficult
       30+	very confusing
    """

    @staticmethod
    def score(document):
        WORDS = document.words
        SENTENCE_COUNT = document.summary.sentences
        WORD_COUNT = document.summary.words
        SHORT_WORD_COUNT = 0.0

        for word in WORDS:
            if len(word.text) < 3:
                SHORT_WORD_COUNT += 1.0

        mcalpine_score = (WORD_COUNT + SHORT_WORD_COUNT) / SENTENCE_COUNT
        return mcalpine_score


class SMOGIndex(Metric):
    """
    Estimates the years of education needed to understand a piece of writing

    http://en.wikipedia.org/wiki/SMOG_(Simple_Measure_Of_Gobbledygook)
    """

    @staticmethod
    def score(document):
        COMPLEX_WORD_COUNT = document.summary.complex_words
        SENTENCE_COUNT = document.summary.sentences

        smog_score = (math.sqrt(COMPLEX_WORD_COUNT * (30 / SENTENCE_COUNT)) + 3)
        return smog_score
