
def contains_of_from(sentence):
    split_s = sentence.lower().split(' ')

    # We want the standalone word form
    if 'of' in split_s or 'from' in split_s:
        return True
    else:
        return False