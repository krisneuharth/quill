"""
Module for determining if a sentence contains
a party that could be better personalized
"""

PARTIES = """
defendant
plaintiff
appellee
guarantor
petitioner
respondent
complainant
witness
lessee
lessor
"""

# Load this dictionary into memory,
# normalize case to lower
PARTIES_DICTIONARY = set(
    line.lower() for line in PARTIES.splitlines()
)
PARTIES_DICTIONARY.remove('')


def is_party(word):
    """
    Helper for a dictionary lookup to
    see if a word is a party or not
    """

    if word.lower() in PARTIES_DICTIONARY:
        return True
    return False


def contains_party(sentence):
    """
    Helper for seeing if a party appears in
    a sentence
    """

    for word in sentence.split(' '):
        if is_party(word):
            return True
    return False
