import nltk
from itertools import dropwhile


# TODO: Fix this?
def contains_passive(sentence):
        """
        Determine if a sentence is passive voice

        http://narorumo.googlecode.com/svn/trunk/passive/passive.py
        """

        tagged_sentence = nltk.pos_tag(
            nltk.word_tokenize(sentence)
        )

        #print tagged_sentence

        post_to_be = list(
            dropwhile(
                lambda(tag): not tag[1].startswith("BE"),
                tagged_sentence
            )
        )

        #print post_to_be

        non_gerund = lambda(tag): tag[1].startswith("V") and not tag[1].startswith("VBG")

        filtered = filter(non_gerund, post_to_be)

        #print filtered

        return any(filtered)