from quill.analyzer.dates import contains_date
from quill.analyzer.homophones import contains_homophone
from quill.analyzer.however import contains_however
from quill.analyzer.latin import contains_latin
from quill.analyzer.legalese import contains_legalese
from quill.analyzer.length import over_sentence_length, over_paragraph_length
from quill.analyzer.of_from import contains_of_from
from quill.analyzer.parties import contains_party
from quill.analyzer.passive import contains_passive
from quill.analyzer.quotations import contains_quotation
from quill.analyzer.this_that import contains_this_that
from quill.analyzer.commas import over_comma_limit

from quill.common.utils import ordinal

from quill.analyzer.metrics import (
    to_grade_level
)


class AtomRule(object):
    abstract = True
    name = None

    def test(self, document):
        raise NotImplementedError

    def applies_to(self):
        raise NotImplementedError

    def message(self):
        raise NotImplementedError

    def __repr__(self):
        return self.__class__.__name__


class DocumentRule(AtomRule):
    def applies_to(self):
        return None


class ParagraphRule(AtomRule):
    pass


class SentenceRule(AtomRule):
    pass


class WordRule(AtomRule):
    pass


#
# Document Rules
#

class MinGradeLevel(DocumentRule):
    def __init__(self):
        self.name = 'Minimum Grade Level'
        self.MIN_GRADE_LEVEL = 23
        self.grade_level = None

    def applies_to(self):
        return None

    def test(self, document):
        ARI = document.readability.ARI
        FKGL = document.readability.FKGL

        self.grade_level = int((ARI + FKGL) / 2)

        # Humanize the grade level
        if self.grade_level == 0:
            self.grade_level = 1

        if self.grade_level <= self.MIN_GRADE_LEVEL:
            return False
        return True

    def message(self):
        values = (
            ordinal(self.grade_level),
            to_grade_level(self.MIN_GRADE_LEVEL),
        )

        message = "We believe this document is too simple. "\
                  "This document is best read by readers with a %s grade reading level. "\
                  "We recommend a minimum reading level of at least %s." % values

        return message


class MaxGradeLevel(DocumentRule):
    def __init__(self):
        self.name = 'Maximum Grade Level'
        self.MAX_GRADE_LEVEL = 40
        self.grade_level = None

    def applies_to(self):
        return None

    def test(self, document):
        ARI = document.readability.ARI
        FKGL = document.readability.FKGL

        self.grade_level = int((ARI + FKGL) / 2)

        # Humanize the grade level
        if self.grade_level == 0:
            self.grade_level = 1

        if self.grade_level >= self.MAX_GRADE_LEVEL:
            return False
        return True

    def message(self):
        values = (
            ordinal(self.grade_level),
            self.MAX_GRADE_LEVEL,
        )

        return "We believe this document is too complex. "\
               "This document is best read by readers with a %s grade reading level. "\
               "We recommend a maximum reading level of %d." % values


class MinComplexity(DocumentRule):
    def __init__(self):
        self.name = 'Minimum Complexity'

        # 60-70	Normal (Easily understood by 13 to 15 year old students
        self.MIN_COMPLEXITY = 60
        self.MAX_COMPLEXITY = 30
        self.complexity = None

    def applies_to(self):
        return None

    def test(self, document):
        self.complexity = int(document.readability.FRE)

        if self.complexity >= self.MIN_COMPLEXITY:
            return False
        return True

    def message(self):
        values = (
            self.complexity,
            self.MAX_COMPLEXITY,
            self.MIN_COMPLEXITY
        )

        return "We believe this document is too simple. "\
               "It scores %d with our complexity tests and "\
               "we recommend a score between %d and %d." % values


class MaxComplexity(DocumentRule):
    def __init__(self):
        self.name = 'Maximum Complexity'

        # 0-30	Very Difficult (best understood by college graduates)
        self.MAX_COMPLEXITY = 30
        self.MIN_COMPLEXITY = 60
        self.complexity = None

    def applies_to(self):
        return None

    def test(self, document):
        self.complexity = int(document.readability.FRE)

        if self.complexity <= self.MAX_COMPLEXITY:
            return False
        return True

    def message(self):
        values = (
            self.complexity,
            self.MAX_COMPLEXITY,
            self.MIN_COMPLEXITY
        )

        return "We believe this document is too complex. "\
               "It scores %d with our complexity tests and "\
               "we recommend a score between %d and %d." % values


#
# Sentence Rules
#


class PassiveVoice(SentenceRule):
    def __init__(self):
        self.name = 'Passive Voice'
        self.max = 1
        self.sentences = []

    def applies_to(self):
        return self.sentences

    def test(self, document):
        for sentence in document.sentences:
            if contains_passive(sentence.text):
                self.sentences.append(sentence)

        if len(self.sentences) >= self.max:
            return False
        return True

    def message(self):
        values = (
            len(self.sentences),
            self.max,
        )

        return "We've detected %d sentences with a passive voice. "\
               "We recommend a maximum number of passive sentences of %d. "\
               "While this can be done for style and emphasis reasons, "\
               "it is best to re-write them in the active voice." % values


class ExcessiveLatin(SentenceRule):
    def __init__(self):
        self.name = 'Latin'
        self.max = 5
        self.sentences = []

    def applies_to(self):
        return self.sentences

    def test(self, document):
        for sentence in document.sentences:
            if contains_latin(sentence.text):
                self.sentences.append(sentence)

        if len(self.sentences) >= self.max:
            return False
        return True

    def message(self):
        values = (
            len(self.sentences),
        )

        return "We've detected this document contains at least %d Latin words or phrases. "\
               "Excessive Latin can drastically affect the readability "\
               "of your document." % values


class ExcessiveDates(SentenceRule):
    def __init__(self):
        self.name = 'Dates'
        self.max = 5
        self.sentences = []

    def applies_to(self):
        return self.sentences

    def test(self, document):
        for sentence in document.sentences:
            if contains_date(sentence.text):
                self.sentences.append(sentence)

        if len(self.sentences) >= self.max:
            return False
        return True

    def message(self):
        values = (
            len(self.sentences),
        )

        return "We've detected this document contains at least %d dates. "\
               "Excessive dates can drastically affect the readability "\
               "of your document." % values


class ExcessiveCommas(SentenceRule):
    def __init__(self):
        self.name = 'Commas'
        self.max_commas = 2
        self.max_sentences = 1
        self.sentences = []

    def applies_to(self):
        return self.sentences

    def test(self, document):
        for sentence in document.sentences:
            if over_comma_limit(sentence.text, self.max_commas):
                self.sentences.append(sentence)

        if len(self.sentences) >= self.max_sentences:
            return False
        return True

    def message(self):
        values = (
            len(self.sentences),
            self.max_commas
        )

        return "We've detected this document contains %d sentences "\
               "with more than %d commas. Sentences with more commas "\
               "can drastically affect the readability of your document." % values


class SentencesTooLong(SentenceRule):
    def __init__(self):
        self.name = 'Sentence Length'
        self.max_length = 25
        self.max_sentences = 5
        self.sentences = []

    def applies_to(self):
        return self.sentences

    def test(self, document):
        for sentence in document.sentences:
            if over_sentence_length(sentence.text, self.max_length):
                self.sentences.append(sentence)

        if len(self.sentences) >= self.max_sentences:
            return False
        return True

    def message(self):
        values = (
            len(self.sentences),
            self.max_sentences,
        )

        return "We've detected this document contains %d sentences "\
               "longer than %d words. Long sentences can drastically "\
               "affect the readability of your document." % values


class Homophones(SentenceRule):
    def __init__(self):
        self.name = 'Homophones'
        self.max_words = 10
        self.sentences = []

    def applies_to(self):
        return self.sentences

    def test(self, document):
        for sentence in document.sentences:
            if contains_homophone(sentence.text):
                self.sentences.append(sentence)

        if len(self.sentences) >= self.max_words:
            return False
        return True

    def message(self):
        values = (
            len(self.sentences),
        )

        return "We've detected this document contains %d homophones. "\
               "We recommend a re-reading of this document to ensure you "\
               "are using the correct words in the marked sentences." % values


class ExcessiveQuotation(SentenceRule):
    def __init__(self):
        self.name = 'Quotations'
        self.max = 1
        self.sentences = []

    def applies_to(self):
        return self.sentences

    def test(self, document):
        for sentence in document.sentences:
            if contains_quotation(sentence.text):
                self.sentences.append(sentence)

        if len(self.sentences) >= self.max:
            return False
        return True

    def message(self):
        values = (
            len(self.sentences),
        )

        return "We've detected this document contains %d quotations."\
               " Too many quotations can drastically "\
               "affect the readability of your document." % values


class ExcessiveOfAndFrom(SentenceRule):
    def __init__(self):
        self.name = 'Of and From'
        self.max = 5
        self.sentences = []

    def applies_to(self):
        return self.sentences

    def test(self, document):
        for sentence in document.sentences:
            if contains_of_from(sentence.text):
                self.sentences.append(sentence)

        if len(self.sentences) >= self.max:
            return False
        return True

    def message(self):
        values = (
            len(self.sentences),
        )

        return "We've detected this document contains %d "\
               "uses of the words 'of' and 'from'. Consider "\
               "re-writing these sentences to be more possessive." % values


class ExcessiveHowever(SentenceRule):
    def __init__(self):
        self.name = 'However'
        self.max = 3
        self.sentences = []

    def applies_to(self):
        return self.sentences

    def test(self, document):
        for sentence in document.sentences:
            if contains_however(sentence.text):
                self.sentences.append(sentence)

        if len(self.sentences) >= self.max:
            return False
        return True

    def message(self):
        values = (
            len(self.sentences),
        )

        return "We've detected this document contains %d "\
               "uses of the word 'however'. Consider "\
               "re-writing these sentences to instead use 'and' or 'but'." % values


class ExcessiveThisAndThat(SentenceRule):
    def __init__(self):
        self.name = 'This and That'
        self.max = 3
        self.sentences = []

    def applies_to(self):
        return self.sentences

    def test(self, document):
        for sentence in document.sentences:
            if contains_this_that(sentence.text):
                self.sentences.append(sentence)

        if len(self.sentences) >= self.max:
            return False
        return True

    def message(self):
        values = (
            len(self.sentences),
        )

        return "We've detected this document contains %d "\
               "uses of the word 'that'. Consider "\
               "re-writing these sentences to instead use 'which'." % values


class ExcessiveNakedParty(SentenceRule):
    def __init__(self):
        self.name = 'Party Names'
        self.max = 3
        self.sentences = []

    def applies_to(self):
        return self.sentences

    def test(self, document):
        for sentence in document.sentences:
            if contains_party(sentence.text):
                self.sentences.append(sentence)

        if len(self.sentences) >= self.max:
            return False
        return True

    def message(self):
        values = (
            len(self.sentences),
        )

        return "We've detected this document contains %d "\
               "instances where a party could be explicitly named to "\
               "improve readability." % values


class ExcessiveLegalese(SentenceRule):
    def __init__(self):
        self.name = 'Legal Jargon'
        self.max = 3
        self.sentences = []

    def applies_to(self):
        return self.sentences

    def test(self, document):
        for sentence in document.sentences:
            if contains_legalese(sentence.text):
                self.sentences.append(sentence)

        if len(self.sentences) >= self.max:
            return False
        return True

    def message(self):
        values = (
            len(self.sentences),
        )

        return "We've detected this document contains %d "\
               "instances of 'legalese' where sentences could be better "\
               "worded to improve readability." % values


#
# Paragraph Rules
#


class ParagraphsTooLong(ParagraphRule):
    def __init__(self):
        self.name = 'Paragraph Length'
        self.max_sentences = 5
        self.max_paragraphs = 3
        self.paragraphs = []

    def applies_to(self):
        return self.paragraphs

    def test(self, document):
        for paragraph in document.paragraphs:
            if over_paragraph_length(document, paragraph, self.max_sentences):
                self.paragraphs.append(paragraph)

        if len(self.paragraphs) >= self.max_paragraphs:
            return False
        return True

    def message(self):
        values = (
            len(self.paragraphs),
            self.max_sentences,
        )

        return "We've detected this document contains %d paragraphs "\
               "longer than %d sentences. Longer paragraphs can drastically "\
               "affect the readability of your document." % values
