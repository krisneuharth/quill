"""
Syllable Counter based on Greg Fast's Perl module:
    Lingua::EN::Syllable
"""

import re

fallback_cache = {}
fallback_subsyl = ["cial", "tia", "cius", "cious", "gui", "ion", "iou",
                   "sia$", ".ely$"]
fallback_addsyl = ["ia", "riet", "dien", "iu", "io", "ii",
                   "[aeiouy]bl$", "mbl$",
                   "[aeiou]{3}",
                   "^mc", "ism$",
                   "(.)(?!\\1)([aeiouy])\\2l$",
                   "[^l]llien",
                   "^coad.", "^coag.", "^coal.", "^coax.",
                   "(.)(?!\\1)[gq]ua(.)(?!\\2)[aeiou]",
                   "dnt$"]


# Compile our regular expressions
for i in range(len(fallback_subsyl)):
    fallback_subsyl[i] = re.compile(fallback_subsyl[i])
for i in range(len(fallback_addsyl)):
    fallback_addsyl[i] = re.compile(fallback_addsyl[i])


def _normalize_word(word):
    if not word:
        return word

    return word.strip().lower()


def is_complex_word(sentence, word, syllables):
    """
    Determine if a word is a complex word
    based on number of syllables and position
    in sentence
    """

    if syllables >= 3:
        # Checking proper nouns. If a word starts with a capital letter
        # and is NOT at the beginning of a sentence we don't add it
        # as a complex word.

        if word[0].isupper() and not sentence.startswith(word):
            return 0

        return 1
    else:
        return 0


def count_syllables(word):
    """
    Count the number of syllables in a word
    """

    word = _normalize_word(word)

    if not word:
        return 0

    # Check for a cached syllable count
    count = fallback_cache.get(word, -1)
    if count > 0:
        return count

    # Remove final silent 'e'
    if word[-1] == "e":
        word = word[:-1]

    # Count vowel groups
    count = 0
    prev_was_vowel = 0
    for c in word:
        is_vowel = c in ("a", "e", "i", "o", "u", "y")
        if is_vowel and not prev_was_vowel:
            count += 1
        prev_was_vowel = is_vowel

    # Add & subtract syllables
    for r in fallback_addsyl:
        if r.search(word):
            count += 1
    for r in fallback_subsyl:
        if r.search(word):
            count -= 1

    # Cache the syllable count
    fallback_cache[word] = count

    return count
