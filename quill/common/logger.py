import logging
from quill.common.settings import LOGGING_FORMAT, DATE_FORMAT

logging.basicConfig(
    format=LOGGING_FORMAT,
    datefmt=DATE_FORMAT,
    level=logging.INFO
)

logger = logging.getLogger('analyzer')

