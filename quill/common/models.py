# -*- coding: utf-8 -*-
import json

import os
import uuid
import string
from datetime import datetime
import docx

from nltk import sent_tokenize

from logger import logger

from quill.analyzer.metrics import (
    ARI, ColemanLiauIndex,
    FleschKincaidGradeLevel,
    FleschReadingEase,
    GunningFogIndex,
    LIX, RIX,
    McAlpineEFLAW,
    SMOGIndex
)

from quill.common import settings
from quill.web import db

from quill.common.recommendations import (
    Recommender
)

from quill.analyzer.syllables import (
    count_syllables, is_complex_word
)


class Atom(db.EmbeddedDocument):
    meta = {
        'allow_inheritance': True
    }

    pid = db.StringField(required=True)
    text = db.StringField(required=True)

    def __str__(self):
        return self.text


class Word(Atom):
    sid = db.StringField(required=True)
    wid = db.StringField(required=True)

    def __repr__(self):
        return json.dumps(dict(
            pid=self.pid,
            sid=self.sid,
            wid=self.wid,
            text=self.text
        ))


class Sentence(Atom):
    sid = db.StringField(required=True)

    def __repr__(self):
        return json.dumps(dict(
            pid=self.pid,
            sid=self.sid,
            text=self.text
        ))


class Paragraph(Atom):

    def __repr__(self):
        return json.dumps(dict(
            pid=self.pid,
            text=self.text
        ))


class Readability(db.EmbeddedDocument):
    ARI = db.FloatField()
    CLI = db.FloatField()
    FKGL = db.FloatField()
    FRE = db.FloatField()
    GFI = db.FloatField()
    LIX = db.FloatField()
    RIX = db.FloatField()
    MEFLAW = db.FloatField()
    SMOG = db.FloatField()

    @classmethod
    def for_document(cls, document):
        return cls(
            ARI=ARI.score(document),
            CLI=ColemanLiauIndex.score(document),
            FKGL=FleschKincaidGradeLevel.score(document),
            FRE=FleschReadingEase.score(document),
            GFI=GunningFogIndex.score(document),
            LIX=LIX.score(document),
            RIX=RIX.score(document),
            MEFLAW=McAlpineEFLAW.score(document),
            SMOG=SMOGIndex.score(document)
        )

    def __repr__(self):
        return json.dumps(dict(
            ARI=self.ARI,
            CLI=self.CLI,
            FKGL=self.FKGL,
            FRE=self.FRE,
            GFI=self.GFI,
            LIX=self.LIX,
            RIX=self.RIX,
            MEFLAW=self.MEFLAW,
            SMOG=self.SMOG
        ))

class Summary(db.EmbeddedDocument):
    # Ints
    paragraphs = db.IntField()
    sentences = db.IntField()
    words = db.IntField()
    syllables = db.IntField()
    complex_words = db.IntField()
    characters = db.IntField()

    # Floats
    avg_words_per_paragraph = db.FloatField()
    avg_words_per_sentence = db.FloatField()
    avg_sentences_per_paragraph = db.FloatField()
    avg_characters_per_word = db.FloatField()
    avg_syllables_per_word = db.FloatField()
    avg_syllables_per_sentence = db.FloatField()
    avg_syllables_per_paragraph = db.FloatField()

    def __repr__(self):
        return json.dumps(dict(
            # Ints
            paragraphs=self.paragraphs,
            sentences=self.sentences,
            words=self.words,
            syllables=self.syllables,
            complex_words=self.complex_words,
            characters=self.characters,
        
            # Floats
            avg_words_per_paragraph=self.avg_words_per_paragraph,
            avg_words_per_sentence=self.avg_words_per_sentence,
            avg_sentences_per_paragraph=self.avg_sentences_per_paragraph,
            avg_characters_per_word=self.avg_characters_per_word,
            avg_syllables_per_word=self.avg_syllables_per_word,
            avg_syllables_per_sentence=self.avg_syllables_per_sentence,
            avg_syllables_per_paragraph=self.avg_syllables_per_paragraph
        ))

class Recommendation(db.EmbeddedDocument):
    name = db.StringField()
    message = db.StringField()
    applies_to = db.ListField(
        db.EmbeddedDocumentField(Atom)
    )

    @classmethod
    def from_rule(cls, rule):
        return cls(
            name=rule.name,
            message=rule.message(),
            applies_to=rule.applies_to()
        )

    def __repr__(self):
        return json.dumps(dict(
            name=self.name,
            message=self.message,
            applies_to=self.applies_to
        ))


class Document(db.Document):
    """
    Document class to contain metrics,
    suggestions, etc.
    """

    meta = {
        'collection': 'documents',
        'indexes': ['user_id']
    }

    id = db.StringField(
        unique=True,
        primary_key=True
    )

    user_id = db.StringField(required=True)
    date_created = db.DateTimeField(default=datetime.now)

    # From file
    filename = db.StringField(required=True)
    extension = db.StringField(required=True)

    # Document Atoms
    full_text = db.StringField()
    paragraphs = db.ListField(
        db.EmbeddedDocumentField(Paragraph)
    )
    sentences = db.ListField(
        db.EmbeddedDocumentField(Sentence)
    )
    words = db.ListField(
        db.EmbeddedDocumentField(Word)
    )

    # Summary
    summary = db.EmbeddedDocumentField(Summary)

    # Readability
    readability = db.EmbeddedDocumentField(Readability)

    # Recommendations
    recommendations = db.ListField(
        db.EmbeddedDocumentField(Recommendation)
    )

    def save(self, *args, **kwargs):
        """
        Override save
        """

        # Don't write out document stuff
        #self.full_text = None
        #self.paragraphs = None
        #self.sentences = None
        #self.words = None

        # Make sure this is unique
        self.id = 'doc-%s' % str(uuid.uuid4())

        super(Document, self).save(*args, **kwargs)

    @classmethod
    def _clean(cls, text):
        """
        Process text
        """

        # Get rid of extra whitespace
        text = text.strip()

        # Replace weird chars
        text = text.replace(u"“", '"').replace(u"”", '"')
        text = text.replace(u"\u2026", '... ')
        text = text.replace(u"\u266a", '')

        # This is actually json escaping the quote
        text = text.replace(u'"', "'")

        return text

    @classmethod
    def _parse(cls, document):
        """
        Convert Word document into our Document representation
        """

        paragraph_count = 0
        sentence_count = 0
        word_count = 0
        char_count = 0
        syllable_count = 0
        complex_word_count = 0

        # Normalize text and doc into list
        paragraphs = [
            p.text
            # p.text.encode('ascii', 'ignore')
            for p in document.paragraphs
        ]

        cleaned_paragraphs = []
        cleaned_sentences = []
        cleaned_words = []

        for paragraph in paragraphs:
            paragraph = cls._clean(paragraph)
            cleaned_paragraphs.append(
                Paragraph(
                    pid=str(paragraph_count),
                    text=paragraph
                )
            )

            sentences = sent_tokenize(paragraph)

            for sentence in sentences:
                #logger.debug('Sentence: ' + repr(sentence))

                cleaned_sentences.append(
                    Sentence(
                        pid=str(paragraph_count),
                        sid=str(sentence_count),
                        text=sentence
                    )
                )

                words = sentence.split(' ')
                for word in words:
                    word = word.strip(string.punctuation)
                    word = word.strip()

                    if word not in string.punctuation:
                        #logger.debug('Word: ' + repr(word))

                        cleaned_words.append(
                            Word(
                                pid=str(paragraph_count),
                                sid=str(sentence_count),
                                wid=str(word_count),
                                text=word
                            )
                        )

                        syllables = count_syllables(word)
                        complex_word_count += is_complex_word(
                            sentence, word, syllables
                        )
                        syllable_count += syllables
                        char_count += len(word)
                        word_count += 1

                sentence_count += 1
                #logger.debug('')

            paragraph_count += 1
        #logger.debug('')

        summary = Summary(
            # Counts
            paragraphs=int(paragraph_count),
            sentences=int(sentence_count),
            words=int(word_count),
            characters=int(char_count),
            syllables=int(syllable_count),
            complex_words=int(complex_word_count),

            # Averages
            avg_words_per_paragraph=float(word_count) / float(paragraph_count),
            avg_words_per_sentence=float(word_count) / float(sentence_count),
            avg_sentences_per_paragraph=float(sentence_count) / float(paragraph_count),
            avg_characters_per_word=float(char_count) / float(word_count),
            avg_syllables_per_word=float(syllable_count) / float(word_count),
            avg_syllables_per_sentence=float(syllable_count) / float(sentence_count),
            avg_syllables_per_paragraph=float(syllable_count) / float(paragraph_count),
        )

        # Create our Document instance
        return cls(
            full_text="\n".join([
                p.text.encode('ascii', 'ignore')
                for p in cleaned_paragraphs
            ]),
            paragraphs=cleaned_paragraphs,
            sentences=cleaned_sentences,
            words=cleaned_words,
            summary=summary
        )

    @classmethod
    def _file_attrs(cls, fs):
        """
        Parse out the file attributes
        """

        try:
            filename, extension = os.path.splitext(
                fs.filename
            )
            extension = extension.replace('.', '').lower()
        except:
            filename = fs.name
            extension = fs.name.split('.')[-1].lower()

        return filename, extension

    @classmethod
    def from_file(cls, user_id, fs):
        """
        Return a Document from a file stream
        """

        try:
            # Convert XML to Document
            doc = cls._parse(
                docx.Document(fs)
            )

            # Just in case?
            fs.close()

            # Set additional attributes
            doc.user_id = user_id
            doc.filename, doc.extension = cls._file_attrs(fs)

            # Get readability summary
            doc.readability = Readability.for_document(doc)

            # Get recommendations for the file
            doc.recommendations = [
                Recommendation.from_rule(rr)
                for rr in Recommender.for_document(
                    doc
                )
            ]

            logger.debug('Recommendations: %s', [
                rec.message for rec in doc.recommendations]
            )

            # Save and clear
            if settings.TEST_RUNNER is False:
                logger.debug('Saving...')
                doc.save()
                logger.debug('Done.')

            return doc
        except Exception as e:
            logger.error(e)
            return None

    # def __str__(self):
    #     return self.full_text

    # def __repr__(self):
    #     return '[D] - %s\n' % self.id
