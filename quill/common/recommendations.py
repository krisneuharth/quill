from multiprocessing import Pool

from quill.analyzer.rules import (
    MinGradeLevel, MaxGradeLevel,
    MinComplexity, MaxComplexity,
    PassiveVoice, ExcessiveLatin,
    ExcessiveDates, ExcessiveCommas,
    ParagraphsTooLong, SentencesTooLong,
    Homophones, ExcessiveQuotation,
    ExcessiveOfAndFrom, ExcessiveHowever,
    ExcessiveThisAndThat, ExcessiveNakedParty,
    ExcessiveLegalese
)

from logger import logger


RULES = (
    MinGradeLevel,
    MaxGradeLevel,
    MinComplexity,
    MaxComplexity,
    PassiveVoice,
    ExcessiveLatin,
    ExcessiveDates,
    ExcessiveCommas,
    ParagraphsTooLong,
    SentencesTooLong,
    Homophones,
    ExcessiveQuotation,
    ExcessiveOfAndFrom,
    ExcessiveHowever,
    ExcessiveThisAndThat,
    ExcessiveNakedParty,
    ExcessiveLegalese
)


class Recommender(object):
    """
    Get recommendations for writing samples
    based on metric scores
    """

    @staticmethod
    def for_document(document):
        """
        Given a document, provide writing suggestions
        """

        recommendations = []
        for cls in RULES:
            rule = cls()

            if not rule.test(document):
                logger.debug('Rule Fail: %s', rule.name)
                recommendations.append(rule)

        return recommendations


def async_process(rule_document):
    """
    Define our process step for multiprocessing map
    """

    cls, document = rule_document

    rule = cls()
    if not rule.test(document):
        logger.debug('Rule Fail: %s', rule.name)
        return rule
    else:
        return None


class MultiprocessingRecommender(Recommender):

    @staticmethod
    def for_document(document):
        """
        Given a document, provide writing suggestions, faster
        """

        # Setup the pool
        pool = Pool(processes=8)

        # Construct our args
        args = zip(
            RULES,
            [document for ii in xrange(len(RULES))]
        )

        # Map
        recommendations = pool.map_async(
            async_process,
            args
        )

        # Reduce
        recommendations = [
            rr for rr in recommendations.get() if rr
        ]

        return recommendations