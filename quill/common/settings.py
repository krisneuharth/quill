from datetime import timedelta

# Session and CSRF key - generated with: import os; os.urandom(24)
SECRET_KEY = '\xbc\xfec\x9b\x1c\x92\xd9\xf8z[{\xd6\xb8\x9b\x8e\x89\xb5\xe9>\xbc\xd1\x17y\xd5'

# Base URL for our assets
ASSETS_BASE_URL = '/assets'

# How long the session is alive
PERMANENT_SESSION_LIFETIME = timedelta(minutes=60)

LOGGING_FORMAT = '%(levelname)s - %(asctime)s - %(message)s'
DATE_FORMAT = '%m/%d/%Y %I:%M:%S %p'


# Mail Sending
MAILGUN_KEY = 'key-9b26tkt3ktm8t11o5v2szgkafe9uinz3'
MAILGUN_DOMAIN = 'quill.mailgun.org'

# Data for testing
DATA_DIR = '/Users/kneuharth/projects/quillapp/quill/analyzer/data/'
DATA_FILE = '/Users/kneuharth/projects/quillapp/quill/analyzer/data/sample1.docx'

# Stripe
TEST_SECRET_KEY = 'sk_test_Wf9rbUh2tfyC2hVRUO72BxG4'
TEST_PUBLISHABLE_KEY = 'pk_test_SjbJmvaPm5PD4AKIESkTNHME'

LIVE_SECRET_KEY = 'sk_live_mzqkh1jFajFcSI08Gco06Eop'
LIVE_PUBLISHABLE_KEY = 'pk_live_cniqTfdgKbfdEGXVqg6Yu2IM'

# MongoDB
MONGODB_SETTINGS = {
    'DB': 'quillapp',
    'HOST': 'mongodb://quillapp:Qu1llaPP@ds027739.mongolab.com:27739/heroku_app21398285',
}

INVITE_CODES = ['test', 'demo', 'hn', 'fb']

TEST_RUNNER = False