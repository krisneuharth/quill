

def ordinal(value):
    """
    Converts an integer to its ordinal as a string. 1 is '1st', 2 is '2nd',
    3 is '3rd', etc. Works for any integer.

    http://hdknr.github.io/docs/django/modules/django/contrib/humanize/templatetags/humanize.html#ordinal
    """

    try:
        value = int(value)
    except (TypeError, ValueError):
        return value

    suffixes = ('th', 'st', 'nd', 'rd', 'th', 'th', 'th', 'th', 'th', 'th')
    if value % 100 in (11, 12, 13):  # special case
        return u"%d%s" % (value, suffixes[0])

    return u"%d%s" % (value, suffixes[value % 10])