import os
import sys

from flask import Flask
from flask.ext.bcrypt import Bcrypt
from flask.ext.mail import Mail
from flask.ext.bootstrap import Bootstrap
from flask.ext.mongoengine import (
    MongoEngine
)

import stripe

ROOT = os.path.dirname(os.path.dirname(__file__))
sys.path.insert(0, ROOT)

# Global references
bcrypt = Bcrypt()
mail = Mail()
bootstrap = Bootstrap()
db = MongoEngine()


def create_app(debug=True):
    """
    Create a Flask app
    """

    # Create base app
    app = Flask(
        __name__,
        template_folder='templates',
        static_folder='assets',
    )

    # Use Login
    from flask.ext.login import LoginManager
    from quill.web.blueprints.account.models import load_user
    login_mgr = LoginManager()
    login_mgr.login_view = 'account.login'
    login_mgr.user_loader(load_user)

    # Read settings
    app.config.from_object('quill.common.settings')
    app.debug = debug

    # Initialize extensions
    db.init_app(app)
    login_mgr.init_app(app)
    bootstrap.init_app(app)
    bcrypt.init_app(app)
    mail.init_app(app)

    # Register blueprints
    from quill.web.blueprints.account.views import account
    app.register_blueprint(account, url_prefix='/account')

    from quill.web.blueprints.admin.views import admin
    app.register_blueprint(admin, url_prefix='/admin')

    from quill.web.blueprints.site.views import site
    app.register_blueprint(site, url_prefix='/')

    from quill.web.blueprints.q.views import q
    app.register_blueprint(q, url_prefix='/app')

    ###### Document APIs
    from quill.web.blueprints.q.views import DocumentAPI
    document_view = DocumentAPI.as_view('document_api')
    app.add_url_rule(
        '/api/documents/',
        defaults={'id': None},
        view_func=document_view,
        methods=['GET', 'DELETE']
    )
    app.add_url_rule(
        '/api/documents/',
        view_func=document_view,
        methods=['POST']
    )
    app.add_url_rule(
        '/api/documents/<string:id>/',
        view_func=document_view,
        methods=['GET', 'DELETE']
    )

    from quill.web.blueprints.profile.views import profile
    app.register_blueprint(profile, url_prefix='/app/profile')

    ###### Profile APIs
    from quill.web.blueprints.account.views import (
        ChangePasswordAPI, DeleteAccountAPI,
        PlanAPI
    )

    app.add_url_rule(
        '/api/password/change/',
        view_func=ChangePasswordAPI.as_view('change_password_api')
    )

    app.add_url_rule(
        '/api/account/delete/',
        view_func=DeleteAccountAPI.as_view('delete_account_api')
    )

    app.add_url_rule(
        '/api/plans/update/',
        view_func=PlanAPI.as_view('plan_update_api')
    )

    # Use Stripe
    if app.debug:
        # TEST
        from quill.common.settings import TEST_SECRET_KEY
        stripe.api_key = TEST_SECRET_KEY
    else:
        # LIVE
        from quill.common.settings import LIVE_SECRET_KEY
        stripe.api_key = LIVE_SECRET_KEY

    # Register Jinja filters
    from quill.web.core import filters
    app.jinja_env.filters['external'] = filters.make_external
    app.jinja_env.filters['date'] = filters.dateformat
    app.jinja_env.filters['datetime'] = filters.datetimeformat
    app.jinja_env.filters['timesince'] = filters.naturaltime

    # Context processors
    from quill.web.core import context_processors
    app.context_processor(context_processors.config)
    app.context_processor(context_processors.scheme)

    return app
