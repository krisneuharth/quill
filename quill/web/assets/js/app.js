
$('.delete-all-btn').on('click', function(e) {
    e.preventDefault();

    $.ajax({
        type: 'DELETE',
        url: "/api/documents/",
        success: function(data){
            var $documents_list = $('.documents-list');

            $documents_list.children().remove();
            $documents_list.append(
                "<li class='warning'>You have no documents yet!</li>"
            );
        },
        error: function(errors){
            console.log(errors);
        }
    });
});

$('body').on('click', ".delete-doc-btn", function(e) {
    e.preventDefault();

    var id = $(this).data('id');

    $.ajax({
        type: 'DELETE',
        url: "/api/documents/" + id + "/",
        success: function(data){
            var $button = $(e.target);
            var $parent = $button.parent();
            var $ul = $parent.parent();

            if ($parent.prop('tagName') == 'LI') {
                $parent.remove();

                if ($ul.children().length === 0) {
                    $ul.append("<li class='warning'>You have no documents yet!</li>");
                } else {
                    $ul.find('.warning').remove();
                }
            }
            else {
                    window.location.replace('/app/');
                }
            },
        error: function(errors){
            console.log(errors);
        }
    });
});


$('#upload').fileupload({
    // This function is called when a file is added to the queue
    add: function (e, data) {
        // Automatically upload the file once it is added to the queue
        $('.analyzing').show();
        var jqXHR = data.submit();
    },
    done: function (e, data) {
        $('.analyzing').hide();

        var source = $("#li-document-template").html();
        var template = Handlebars.compile(source);

        var li = template({
            id: data.result._id,
            filename: data.result.filename
        });

        var $documents_list = $('.documents-list');
        $documents_list.find('.warning').remove();
        $documents_list.prepend(li);

        console.log(data.result._id);
    },
    fail: function (e, data) {
        console.log('FAIL');
        console.log(data);

        $('.analyzing').hide();
    }
});

///////////////////////////////////////////////////////////////////

//
// Models and Collections
//


    var config = {
        "urlRoot": "/api/documents/"
    };


    var Document = Backbone.Model.extend({
        //urlRoot: config.urlRoot
//        defaults: {
//            recommendations: [],
//            summary: [],
//            paragraphs: [],
//            sentences: []
//        }
    });

//    var Paragraph = Backbone.Model.extend({
//
//    });
//
//    var Sentence = Backbone.Model.extend({
//
//    });
//
//    var Summary = Backbone.Model.extend({
//
//    });
//
//    var Paragraph = Backbone.Collection.extend({
//        // Reference to this collection's model.
//        model: Sentence
//    });


//    var Documents = Backbone.Collection.extend({
//        "asc" : false,
//        "url" : config.documents.url,
//        "model" : Document,
//        "initialize" : function(){}
//    });


    //
    // Views
    //

    var AppView = Backbone.View.extend({
        "el" : $('#viewer'),
        "events": {
            "click": 'onClick',
            "mouseover": 'onHover',
            "mouseout": 'onLeave',
            "mousemove": 'onMove'
        },
        "initialize": function(options) {
            var closure = this;

            this.options = options;
            console.log(this.options);

            _.bindAll(
                this,
                "render",
                "onClick",
                "onHover",
                "onMove"
            );

            this.$el = $(this.el);

            // Create a new doc to render
            this.doc = new Document({
                'id': this.options.id,
                'paragraphs': this.options.paragraphs,
                'sentences': this.options.sentences,
                'recommendations': this.options.recommendations
            });

            this.render();
        },
        "onClick": function(e) {
            this.$el.css('background-color', 'red');
        },
        "onHover": function(e) {
            this.$el.css('background-color', 'yellow');
        },
        "onLeave": function(e) {
            //console.log('mouse over');
            this.$el.css('background-color', 'white');
        },
        "onMove": function(e) {

        },
        "render": function() {
            return this;
        }
    });

