import uuid
from flask import render_template, url_for

from quill.web.core.utils import send_mail


def send_activation_email(user):
    """
    Send an activation email with a token
    """

    # Generate an activation code
    user.activation_code = 'aid-' + str(uuid.uuid4())

    # Save it with the user
    user.save()

    # Generate activation link
    context = {
        'link': '%s?code=%s' % (
            url_for('account.activate'),
            user.activation_code
        )
    }

    send_mail(
        user.email_address,
        'accounts@usequill.com',
        'Quill Account Activation',
        render_template('emails/text_activation_email.html', **context),
        render_template('emails/html_activation_email.html', **context)
    )


def send_password_reset_email(user, password):
    """
    Send a password reset email with new password
    """

    context = {
        'password': password
    }

    send_mail(
        user.email_address,
        'accounts@usequill.com',
        'Quill Password Reset',
        render_template('emails/text_password_reset_email.html', **context),
        render_template('emails/html_password_reset_email.html', **context)
    )
