from flask.ext.wtf import Form, TextField, BooleanField
from flask.ext.wtf import Required
import re
from wtforms import PasswordField
from wtforms.validators import Email, EqualTo, ValidationError

from quill.web.blueprints.account.messages import (
    MSG_WEAK_PASSWORD, MSG_PASSWORDS_MUST_MATCH
)


class StrongPassword(object):
    """
    Validator that checks that a password is strong
    """

    def __init__(self, message=None):
        if not message:
            message = MSG_WEAK_PASSWORD
        self.message = message

    def __call__(self, form, field):
        """
        Validate the field
        """

        if not self.is_strong_password(field.data):
            raise ValidationError(self.message)

    def is_strong_password(self, password):
        """
        Determine if a password is strong

        Current rules:
            Password must be a minimum of six characters in length,
            at least one letter and one number,
            and contain at least four different characters
        """

        return bool(
            len(password) >= 6 and
            re.findall('\d', password) and
            re.findall('[A-Za-z]', password) and
            len(set(password)) >= 4
        )


class LoginForm(Form):
    email_address = TextField(
        'Email Address',
        validators=[
            Required(),
            Email()
        ]
    )

    password = PasswordField('Password', validators=[Required()])
    remember_me = BooleanField('Remember Me', default=False)


class ChangePasswordForm(Form):
    password = PasswordField(
        'New Password',
        validators=[
            Required(),
            EqualTo('password_confirm', message=MSG_PASSWORDS_MUST_MATCH),
            StrongPassword()
        ]
    )

    password_confirm = PasswordField(
        'Confirm New Password',
        validators=[
            Required(),
            StrongPassword()
        ]
    )


class ResetPasswordForm(Form):
    email_address = TextField(
        'Email Address',
        validators=[
            Required()
        ]
    )


class SignupForm(Form):
    email_address = TextField(
        'Email Address',
        validators=[
            Required(),
            Email()
        ]
    )

    password = PasswordField(
        'Password',
        validators=[
            Required(),
            EqualTo('password_confirm', message=MSG_PASSWORDS_MUST_MATCH),
            StrongPassword()
        ]
    )

    password_confirm = PasswordField(
        'Confirm Password',
        validators=[
            Required(),
            StrongPassword()
        ]
    )


class SignupInviteCodeForm(SignupForm):
    invite_code = TextField(
        'Invite Code',
        validators=[
            Required()
        ]
    )


class DeleteAccountForm(Form):
    acknowledgement = BooleanField(
        'I acknowledge that deleting my account is permanent.',
        default=False,
        validators=[
            Required()
        ]
    )


class ActivationCodeForm(Form):
    activation_code = TextField(
        'Activation Code',
        validators=[
            Required()
        ]
    )


class WelcomeCodeForm(Form):
    welcome_code = TextField(
        'Welcome Code',
        validators=[
            Required()
        ]
    )
