MSG_NONE = None

# Users
MSG_USER_NOT_FOUND = 'A user with this email address can not be found.'
MSG_USER_ACCOUNT_EXISTS = 'Did you already sign up for an account with this email address?'
MSG_USER_IS_INACTIVE = 'This account is valid but not currently active.'
MSG_USER_ACTIVATED = 'Your account is now active. Please login.'
MSG_USER_LOGIN = 'Welcome back! Upload a document or paste text and let\'s get started.'

# Activation
MSG_INVALID_ACTIVATION_CODE = 'This activation code is invalid or has expired.'
MSG_MISSING_ACTIVATION_CODE = 'Please check your email for the activation code.'

# Invite / Welcome
MSG_INVALID_INVITE_CODE = 'This invite code is invalid or has expired.'

# Passwords
MSG_INVALID_PASSWORD = 'Invalid password for this user.'
MSG_WEAK_PASSWORD = 'Password is weak.'
MSG_PASSWORDS_MUST_MATCH = 'Passwords must match.'
MSG_PASSWORD_CHANGED_SUCCESSFULLY = 'Your password has been changed.'
MSG_PASSWORD_RESET_SUCCESSFULLY = 'Your password has been reset. Please check your email for the new password.'

MSG_WELCOME_MESSAGE = 'Welcome to Quill! We are pleased to have you as a user.'
MSG_UPDATE_PASSWORD = 'You requested a password change.'

# Billing
MSG_INVALID_PLAN = 'Invalid billing plan.'
