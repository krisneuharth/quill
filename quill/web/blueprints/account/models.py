from datetime import datetime
import uuid
import stripe
from quill.common.settings import INVITE_CODES

from quill.web import bcrypt, db
from flask.ext.login import UserMixin

from quill.web.blueprints.account.emails import (
    send_password_reset_email,
    send_activation_email
)

from quill.web.blueprints.account.messages import (
    MSG_USER_NOT_FOUND, MSG_INVALID_PASSWORD,
    MSG_INVALID_INVITE_CODE, MSG_USER_ACCOUNT_EXISTS,
    MSG_NONE, MSG_PASSWORD_CHANGED_SUCCESSFULLY,
    MSG_PASSWORD_RESET_SUCCESSFULLY,
    MSG_USER_IS_INACTIVE, MSG_UPDATE_PASSWORD,
    MSG_WELCOME_MESSAGE,
    MSG_INVALID_PLAN
)

# Plans
TIER_SUMMER = 0
TIER_ASSOCIATE = 1
TIER_MID_LEVEL = 2
TIER_PARTNER = 3


class User(db.DynamicDocument, UserMixin):
    """
    User class to represent all users of the system
    """

    meta = {
        'collection': 'users',
        'indexes': ['email_address']
    }

    id = db.StringField(
        default='user-%s' % str(uuid.uuid4()),
        unique=True,
        primary_key=True
    )

    admin = db.BooleanField(default=False)
    active = db.BooleanField(default=True)
    email_address = db.EmailField(
        required=True,
        unique=True,
    )
    password = db.StringField(required=True)

    date_last_login = db.DateTimeField(default=None)
    date_joined = db.DateTimeField(default=datetime.now)
    date_cancel = db.DateTimeField(default=None)

    invite_code = db.StringField(default=None)
    activation_code = db.StringField(default=None)

    tier = db.IntField(default=TIER_SUMMER)
    stripe_id = db.StringField(default=None)

    def get_id(self):
        """
        Override the default mixin method
        """

        return str(self.id)

    def is_active(self):
        """
        Override the default mixin method
        that always returns True so we can
        use it elsewhere
        """

        return self.active

    def on_paid_plan(self):
        return self.tier in [
            TIER_ASSOCIATE, TIER_MID_LEVEL, TIER_PARTNER
        ]

    def on_summer_plan(self):
        return self.tier == TIER_SUMMER

    def on_associate_plan(self):
        return self.tier == TIER_ASSOCIATE

    def on_midlevel_plan(self):
        return self.tier == TIER_MID_LEVEL

    def on_partner_plan(self):
        return self.tier == TIER_PARTNER

    def __repr__(self):
        return '<User %r (%s)>' % (
            self.email_address, self.active
        )

    def __str__(self):
        return '<User %r (%s)>' % (
            self.email_address, self.active
        )


def load_user(id):
    """
    Implement the required method for Flask-Login
    to load our user by id
    """

    return User.objects(
        id=id
    ).first()


def get_user(email_address):
    """
    Find a user by email address
    """

    return User.objects(
        email_address=email_address
    ).first()


def get_user_by_activation_code(activation_code):
    """
    Find a user by their activation code
    """

    return User.objects(
        activation_code=activation_code
    ).first()


def authenticate(email_address, password):
    """
    Authenticate a user's credentials
    """

    # Get user with this email address
    user = get_user(email_address)

    if not user:
        return None, MSG_USER_NOT_FOUND

    if not user.is_active():
        return None, MSG_USER_IS_INACTIVE

    # Compare the hash and plaintext
    if bcrypt.check_password_hash(user.password, password):
        # Update last login
        user.date_last_login = datetime.now()
        user.save()

        return user, None
    else:
        return None, MSG_INVALID_PASSWORD


def create_user(email_address, password, invite_code=None):
    """
    Create a user
    """

    user = get_user(email_address)

    if not user:
        # If invite code, validate
        if invite_code and not is_valid_invite_code(invite_code):
            return None, MSG_INVALID_INVITE_CODE

        # User does not exist
        user = User(
            email_address=email_address,
            password=bcrypt.generate_password_hash(password),
            invite_code=invite_code,
        )
        user.save()
        message = MSG_NONE

        # Send an email to activate their account
        send_activation_email(user)
    else:
        # User already exists
        user = None
        message = MSG_USER_ACCOUNT_EXISTS

    return user, message


def delete_account(email_address):
    """
    Delete a user's account
    """

    # Get user with this email address
    user = get_user(email_address)

    if not user:
        return False, MSG_USER_NOT_FOUND

    # Cancel Stripe
    if user.stripe_id:
        customer = stripe.Customer.retrieve(user.stripe_id)
        customer.cancel_subscription()
        user.stripe_id = None

    # Soft delete
    user.active = False
    user.date_cancel = datetime.now()
    user.save()

    return True, MSG_NONE


def is_valid_invite_code(code):
    """
    Check if the invite code is valid
    """

    if str(code).lower() in INVITE_CODES:
        return True
    else:
        return False


def is_valid_activation_code(code):
    """
    Check if the activation code is correct
    """

    user = get_user_by_activation_code(code)

    # Couldn't find the user
    if not user:
        return False
    else:
        return True


def update_password(email_address, password, reset=False):
    """
    Update the user's password, from change or reset
    """

    # Get user with this email address
    user = get_user(email_address)

    if not user:
        return False, MSG_USER_NOT_FOUND

    if reset:
        # Use the first 10 chars of a UUID
        password = str(uuid.uuid4()).replace('-', '')[0:10]

        # Send them a password reset email with plaintext
        send_password_reset_email(user, password)
        message = MSG_PASSWORD_RESET_SUCCESSFULLY
    else:
        message = MSG_PASSWORD_CHANGED_SUCCESSFULLY

    # Save the hash of plaintext
    user.password = bcrypt.generate_password_hash(password)
    user.save()

    return True, message


def update_plan(email_address, plan, card):
    """
    Update the user's billing plan
    """

    # Make sure this is a valid plan
    if plan not in [TIER_SUMMER, TIER_ASSOCIATE,
                    TIER_MID_LEVEL, TIER_PARTNER]:
        return False, MSG_INVALID_PLAN

    # Get user with this email address
    user = get_user(email_address)

    if not user:
        return False, MSG_USER_NOT_FOUND

    if user.stripe_id:
        # If currently subscribed, get the customer object
        customer = stripe.Customer.retrieve(user.stripe_id)
    else:
        # Create a new customer object
        customer = stripe.Customer.create(
            card=card,
            email=email_address,
            plan=get_plan_name(plan)
        )

    # Update the customer's subscription plan
    if plan == TIER_SUMMER:
        # If downgrading, cancel the subscription
        customer.cancel_subscription()
        user.stripe_id = None
    else:
        # Any other plan, update them
        customer.update_subscription(plan=get_plan_name(plan))
        user.stripe_id = customer.id

    # Update the user's plan and id
    user.tier = plan
    user.save()

    return True, MSG_NONE


def get_plan_name(tier):
    """
    Get the plan name given a tier
    """

    if tier == TIER_SUMMER:
        return 'summer_associate'
    elif tier == TIER_ASSOCIATE:
        return 'associate'
    elif tier == TIER_MID_LEVEL:
        return 'mid_level'
    elif tier == TIER_PARTNER:
        return 'partner'
    else:
        return None
