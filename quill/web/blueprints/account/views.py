from . import account

from flask import (
    render_template, redirect,
    url_for, request, session, flash,
)

from flask.ext.login import (
    logout_user,
    login_required,
    login_user,
)

from flask.views import MethodView
from quill.web.core.responses import (
    Success, InternalServerError, BadRequest
)

from quill.web.blueprints.account.forms import (
    LoginForm, ChangePasswordForm,
    ResetPasswordForm, SignupForm,
    SignupInviteCodeForm,
    ActivationCodeForm,
    DeleteAccountForm
)

from quill.web.blueprints.account.messages import (
    MSG_INVALID_ACTIVATION_CODE,
    MSG_USER_ACTIVATED, MSG_USER_LOGIN,
    MSG_MISSING_ACTIVATION_CODE
)

from quill.web.blueprints.account.models import (
    authenticate, create_user,
    is_valid_activation_code, delete_account,
    update_password,
    update_plan
)


@account.route('/signup', methods=['GET', 'POST'])
def signup():
    """
    Provide a view for a new user to signup
    """

    # Bind the form to the request
    form = SignupForm(request.form)

    if form.validate_on_submit():
        # Get form fields
        email_address = form.email_address.data
        password = form.password.data

        # Create the new user
        user, message = create_user(email_address, password)

        if user:
            # Direct user to activate their account
            return redirect(url_for('account.activate'))
        else:
            # Flash the reason the account was not created
            flash(message, category="alert-error")

    return render_template('signup.html', form=form)


@account.route('/signup-code', methods=['GET', 'POST'])
def signup_with_code():
    """
    Provide a view for a new user
    to signup with a code
    """

    # Bind the form to the request
    form = SignupInviteCodeForm(request.form)

    if form.validate_on_submit():
        # Get form fields
        email_address = form.email_address.data
        password = form.password.data
        invite_code = form.invite_code.data

        # Create the new user
        user, message = create_user(email_address, password, invite_code)

        if user:
            # Direct user to activate their account
            return redirect(url_for('account.activate'))
        else:
            # Flash the reason the account was not created
            flash(message, category="alert-error")

    return render_template('signup_with_code.html', form=form)


@account.route('/activate', methods=['GET', 'POST'])
def activate():
    """
    Provide a view for a user to activate their account
    via email link or manually
    """

    # Bind the form to the request
    form = ActivationCodeForm(request.form)

    activation_code = None
    if request.method == "GET":
        # Handle the GET case where there may be
        # a code from an email link
        activation_code = request.args.get('code', None)

    elif form.validate_on_submit():
        # Get the code from the POST
        activation_code = form.activation_code.data

    # If no code at this point, remind the user to check their email
    if not activation_code:
        flash(MSG_MISSING_ACTIVATION_CODE, category='alert-info')
        context = {"form": form}

        return render_template('activate.html', **context)

    # Validate the code
    if is_valid_activation_code(activation_code):
        # Success: Either log the person in for
        # the first time, or redirect them to the login page

        flash(MSG_USER_ACTIVATED, category='alert-success')
        return redirect(url_for('account.login'))
    else:
        # Error
        flash(MSG_INVALID_ACTIVATION_CODE, category='alert-error')

    context = {"form": form}

    return render_template('activate.html', **context)


@account.route('/login', methods=['GET', 'POST'])
def login():
    """
    Provide a view for a user to login
    """

    # Bind the form to the request
    form = LoginForm(request.form)

    if form.validate_on_submit():
        # Get form fields
        email_address = form.email_address.data
        password = form.password.data
        remember_me = form.remember_me.data

        # Authenticate the user
        user, message = authenticate(email_address, password)

        # If the user is valid, let them in
        if user and login_user(user, remember=remember_me):
            # Set additional session variables
            # TODO: Encrypt the session
            session['email_address'] = user.email_address

            session.modified = True
            session.permanent = True

            flash(MSG_USER_LOGIN, category='alert-success')

            #if user.admin:
                # Redirect the user to the admin
            #    return redirect(url_for('admin.dashboard'))

            # Redirect the user to the app
            return redirect(url_for('q.index'))
        else:
            # Flash the reason they were not logged in
            flash(message, category="alert-error")

    return render_template('login.html', form=form)


@account.route('/password/change', methods=['GET', 'POST'])
@login_required
def change_password():
    """
    Allow a user to change their password if they know
    their existing password
    """

    # Bind the form to the request
    form = ChangePasswordForm(request.form)

    if form.validate_on_submit():
        # Get form fields
        password = form.password.data

        is_changed, message = update_password(
            session['email_address'], password
        )

        if not is_changed:
            # Flash the reason the password was not changed
            flash(message, category="alert-error")
            show_form = True
        else:
            flash(message, category="alert-success")
            show_form = False
    else:
        show_form = True

    context = {
        'show_form': True if request.method == 'GET' else show_form,
        'form': form
    }

    return render_template('change_password.html', **context)


@account.route('/password/reset', methods=['GET', 'POST'])
def reset_password():
    """
    Allow a user to reset their password if they have
    forgotten it
    """

    # Bind the form to the request
    form = ResetPasswordForm(request.form)

    if form.validate_on_submit():
        # Get form fields
        email_address = form.email_address.data

        is_reset, message = update_password(
            email_address, None, reset=True
        )

        if not is_reset:
            # Flash the reason the password was not reset
            flash(message, category="alert-error")
            show_form = True
        else:
            flash(message, category="alert-success")
            show_form = False

    context = {
        'show_form': True if request.method == 'GET' else show_form,
        'form': form
    }

    return render_template('reset_password.html', **context)


@account.route('/logout', methods=['GET'])
@login_required
def logout():
    """
    Log a user out of the system
    """

    logout_user()
    session.clear()

    return redirect(url_for('site.index'))

# TODO: Make this an API
@account.route('/delete', methods=['GET', 'POST'])
@login_required
def delete():
    """
    Provide a view for an existing user to delete their account
    """

    # Bind the form to the request
    form = DeleteAccountForm(request.form)

    if form.validate_on_submit():
        email_address = session['email_address']
        password = form.password.data

        # Delete the user account
        is_deleted, message = delete_account(email_address, password)

        if not is_deleted:
            # Flash the reason they were not cancelled
            flash(message, category="alert-error")
        else:
            # Log them out, clear the session
            logout_user()
            session.clear()

            return redirect(url_for('site.index'))

    return render_template('delete.html', form=form)


#
# API Endpoints (JSON)
#

class ChangePasswordAPI(MethodView):
    decorators = [login_required]

    def put(self):
        form = ChangePasswordForm(request.form)

        if form.validate():
            # Get form fields
            password = form.password.data

            is_changed, message = update_password(
                session['email_address'], password
            )
            if is_changed:
                return Success()
            else:
                return InternalServerError(message)
        else:
            return BadRequest(form.errors)


class PlanAPI(MethodView):
    decorators = [login_required]

    def _charge(self, plan, token):
        is_charged, message = update_plan(
            session['email_address'],
            int(plan),
            token
        )

        if is_charged:
            return Success()
        else:
            return InternalServerError(message)

    def put(self):
        return self._charge(
            request.form.get('planType', None),
            request.form.get('stripeToken', None)
        )

    def post(self):
        return self._charge(
            request.form.get('planType', None),
            request.form.get('stripeToken', None)
        )


class DeleteAccountAPI(MethodView):
    decorators = [login_required]

    def delete(self):
        email_address = session['email_address']

        # Delete the user account
        is_deleted, message = delete_account(email_address)

        if is_deleted:
            # Log them out, clear the session
            logout_user()
            session.clear()

            return Success()
        else:
            return InternalServerError(message)
