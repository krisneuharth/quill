from datetime import datetime, timedelta
import stripe
from mongoengine import Q

from quill.web.blueprints.account.models import (
    User, TIER_SUMMER, TIER_ASSOCIATE,
    TIER_MID_LEVEL, TIER_PARTNER
)


def get_admins():
    # Active, admin
    return User.objects(
        active=True,
        #admin=True
    )


def get_all_users():
    # Active, non-admin
    return User.objects(
        active=True,
        #admin=False
    )


def get_new_users():
    # Active, non-admin, last 30 days
    return User.objects(
        Q(active=True) &
        #Q(admin=False) &
        Q(date_joined__gte=datetime.now() - timedelta(days=30))
    )


def get_never_activated_users():
    # Never active, not cancelled, with activation code
    return User.objects(
        Q(active=False) &
        Q(date_cancel__ne=None) &
        #Q(admin=False) &
        Q(activation_code__ne=None)
    )


def get_cancelled_users():
    # Cancelled, last 30 days
    return User.objects(
        #Q(admin=False) &
        Q(date_cancel__gte=datetime.now() - timedelta(days=30))
    )


def get_invited_users():
    # Active, invited, last 30 days
    return User.objects(
        Q(active=True) &
        #Q(admin=False) &
        Q(invite_code__ne=None) &
        Q(date_joined__gte=datetime.now() - timedelta(days=30))
    )


def get_plan_breakdown():
    # Count of users per tier
    for tier in [TIER_SUMMER, TIER_ASSOCIATE,
                 TIER_MID_LEVEL, TIER_PARTNER]:
        yield tier, User.objects(
            Q(active=True) &
            #Q(admin=False) &
            Q(tier=tier)
        ).count()


def get_paying_users():
    # Count of users at paid tiers
    return User.objects(
        Q(active=True) &
        #Q(admin=False) &
        Q(tier__in=[
            TIER_ASSOCIATE,
            TIER_MID_LEVEL,
            TIER_PARTNER
        ])
    ).count()



