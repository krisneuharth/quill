from . import admin

from flask import (
    render_template, redirect,
    url_for, request, session, flash,
)
from flask.ext.login import login_required
from quill.web.blueprints.admin.models import (
    get_all_users,
    get_new_users,
    get_admins, get_plan_breakdown,
    get_cancelled_users, get_invited_users,
    get_paying_users, get_never_activated_users)


@admin.route('/', methods=['GET', 'POST'])
@login_required
def dashboard():
    """
    Provide an admin dashboard
    """

    context = {
        'admins': get_admins(),
        'all_users': get_all_users(),
        'never_activated_users': get_never_activated_users(),
        'new_users': get_new_users(),
        'invited_users': get_invited_users(),
        'cancelled_users': get_cancelled_users(),
        'paying_users': get_paying_users(),
        'plan_breakdown': get_plan_breakdown()

        # TODO:
        # estimated monthly revenue
        # num documents read (overall, today, yesterday, week, month)

    }

    return render_template('dashboard.html', **context)