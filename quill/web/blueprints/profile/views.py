from . import profile
from flask import render_template
from flask.ext.login import login_required


@profile.route("/")
@login_required
def index():
    """
    Serve the account settings
    """

    context = {}
    return render_template('profile.html', **context)