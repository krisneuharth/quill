from flask import Blueprint

q = Blueprint(
    'q',
    __name__,
    template_folder='templates',
    static_folder='assets'
)
