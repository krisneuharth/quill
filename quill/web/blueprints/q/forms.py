import os

from flask.ext.wtf import Form, FileField
from flask.ext.wtf import Required
from wtforms import ValidationError

EMPTY_LABEL = ''


class UploadDocumentForm(Form):

    file = FileField(
        EMPTY_LABEL,
        validators=[
            Required(),
        ]
    )

    def validate_file(form, field):
        """
        Validate the Word or text extension
        """

        if field.data:
            name, extension = os.path.splitext(
                str(field.data).lower()
            )

            if not extension in ['.doc', '.docx', '.txt']:
                raise ValidationError("Invalid file type: '%s'" % extension)
        else:
            raise ValidationError('Missing file data.')
