from quill.common.models import Document


def get_user_documents(user_id, limit=None):
    """
    Get all of the documents for a user
    """

    results = Document.objects(
        user_id=user_id
    ).order_by('-date_created')

    if limit:
        # Apply the limit
        results = results[:limit]

    return results


def get_user_document(user_id, doc_id):
    """
    Get a single document for a user
    """

    return Document.objects(
        id=doc_id,
        user_id=user_id
    ).first()


def delete_user_documents(user_id):
    """
    Delete all of the documents for a user
    """

    return Document.objects(
        user_id=user_id
    ).delete()


def delete_user_document(user_id, doc_id):
    """
    Delete a document for a user
    """

    return Document.objects(
        id=doc_id,
        user_id=user_id
    ).delete()