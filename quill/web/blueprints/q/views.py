from . import q

from flask import render_template, request, current_app as app
from flask.ext.login import login_required, current_user
from flask.views import MethodView

from quill.common.models import Document
from quill.web.blueprints.q.forms import UploadDocumentForm
from quill.web.blueprints.q.models import (
    get_user_documents, get_user_document,
    delete_user_documents, delete_user_document,
)
from quill.web.core.responses import (
    Success, InternalServerError,
    NotFound, BadRequest
)

@q.route("/")
@login_required
def index():
    """
    Serve the app context
    """

    context = {
        'user': current_user,
        'form': UploadDocumentForm(),
        'documents': get_user_documents(
            current_user.id, limit=5
        )
    }
    return render_template('app.html', **context)


@q.route("/documents")
@login_required
def documents():
    """
    Load the documents for a user
    """

    context = {
        'user': current_user,
        'documents': get_user_documents(
            current_user.id
        )
    }
    return render_template('documents.html', **context)


@q.route("/documents/<string:id>")
@login_required
def document(id):
    """
    Load the document for a user
    """

    context = {
        'user': current_user,
        'doc': get_user_document(
            current_user.id, id
        )
    }
    return render_template('document.html', **context)

@q.route("/help")
@login_required
def help():
    """
    Provide help to the user
    """

    context = {}
    return render_template('help.html', **context)


#
# API Endpoints (JSON)
#

class DocumentAPI(MethodView):
    decorators = [login_required]

    def get(self, id):
        try:
            if id is None:
                # Return the list
                result = get_user_documents(
                    current_user.id
                )
            else:
                # Return the single document
                result = get_user_document(
                    current_user.id, id
                )

            if result:
                return Success(result.to_json())
            else:
                return NotFound(errors=None)
        except Exception as e:
            app.logger.error(e)
            return InternalServerError(e)

    def delete(self, id=None):
        try:
            if id is None:
                # Delete the list
                delete_user_documents(
                    current_user.id
                )
            else:
                # Delete the single document
                delete_user_document(
                    current_user.id, id
                )

            return Success()
        except Exception as e:
            app.logger.error(e)
            return InternalServerError(e)

    def post(self):
        form = UploadDocumentForm(request.form)
        app.logger.debug('Files: %s', request.files)

        if 'file' in request.files:
            try:
                # Get the file, parse it
                # and get recommendations
                doc = Document.from_file(
                    current_user.id,
                    request.files['file']
                )

                app.logger.debug(
                    doc.to_json(indent=2)
                )

                # Return the recommendations
                return Success(doc.to_json())
            except Exception as e:
                app.logger.error(e)
                return InternalServerError(str(e))
        else:
            app.logger.error(form.errors)
            return BadRequest(form.errors)
