from . import site
from flask import render_template, make_response


@site.route("robots.txt")
def robots():
    r = make_response("User-agent: *\nAllow: /")
    r.headers['content-type'] = 'text/plain'
    return r


@site.route("/")
def index():
    return render_template('index.html')
