from flask import current_app, request
from flask.ext.login import current_user


def config():
    """
    Adds current_app.config to context
    """

    return dict(config=current_app.config)


def scheme():
    """
    Adds scheme to the context, https if request is secure, http otherwise
    """

    return dict(scheme="https" if request.is_secure else "http")
