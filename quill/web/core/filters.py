from urlparse import urljoin
from flask import request
import humanize


def make_external(url):
    return urljoin(request.url_root, url)


def _format(value, format):
    try:
        return value.strftime(format)
    except Exception:
        return ''


def dateformat(value, default_format='%-m-%-d-%Y'):
    """
    Format date time: MM-DD-YY
    """

    return _format(value, default_format)


def datetimeformat(value, default_format='%b %-d, %Y %-I:%M %p'):
    """
    Format date time: Mar 2, 2014 9:09am
    """

    return _format(value, default_format)


def naturaltime(datetime):
    return humanize.naturaltime(datetime)
