"""
Helpers for constructing JSON responses, not complete
HTTP 1.1, just the useful ones
"""

import json
from flask.helpers import make_response


def _response(content, code, pretty_print=False):
    """
    Helper to construct a JSON response
    """

    # Don't dumps existing strings
    if not isinstance(content, str):
        if pretty_print:
            content = json.dumps(content, sort_keys=True, indent=4)
        else:
            content = json.dumps(content)

    response = make_response(content)
    response.content_type = 'application/json'
    response.status_code = code

    return response


def Success(content={}, pretty_print=False):
    return _response(content, 200, pretty_print)


def Created(content={}, pretty_print=False):
    return _response(content, 201, pretty_print)


def Accepted(content={}, pretty_print=False):
    return _response(content, 202, pretty_print)


def NoContent(pretty_print=False):
    return _response({}, 204, False)


def BadRequest(errors, pretty_print=False):
    return _response({'errors': errors}, 400, pretty_print)


def Unauthorized(errors, pretty_print=False):
    return _response({'errors': errors}, 401, pretty_print)


def PaymentRequired(errors, pretty_print=False):
    return _response({'errors': errors}, 402, pretty_print)


def Forbidden(errors, pretty_print=False):
    return _response({'errors': errors}, 403, pretty_print)


def NotFound(errors, pretty_print=False):
    return _response({'errors': errors}, 404, pretty_print)


def Conflict(errors, pretty_print=False):
    return _response({'errors': errors}, 409, pretty_print)


def InternalServerError(errors, pretty_print=False):
    return _response({'errors': errors}, 500, pretty_print)


def NotImplemented(errors, pretty_print=False):
    return _response({'errors': errors}, 501, pretty_print)


def ServiceUnavailable(errors, pretty_print=False):
    return _response({'errors': errors}, 503, pretty_print)
