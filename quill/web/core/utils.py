from urlparse import urljoin
from flask import request
import requests

from quill.common import settings


def make_external(url):
    """
    Generate a fully qualified url
    """

    return urljoin(request.url_root, url)


def datetime_handler(obj):
    """
    Default serialization for datetimes
    """

    if hasattr(obj, 'isoformat'):
        return obj.isoformat()


def send_mail(to_address, from_address, subject, plaintext, html):
    r = requests.post(
        "https://api.mailgun.net/v2/%s/messages" % settings.MAILGUN_DOMAIN,
        auth=("api", settings.MAILGUN_KEY),
        data={
            "from": from_address,
            "to": to_address,
            "subject": subject,
            "text": plaintext,
            "html": html
        }
    )

    return r
