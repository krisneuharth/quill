from setuptools import setup, find_packages


setup(
    name="quill",
    version="0.0.1",
    description="usequill.com",
    url="https://github.com/krisneuharth/quillapp",
    long_description="",
    author="krisneuharth",
    author_email="kris.neuharth@gmail.com",
    maintainer="krisneuharth",
    maintainer_email="kris.neuharth@gmail.com",
    license="Other/Proprietary",
    packages=find_packages(exclude=("tests",)),
    test_suite="nose.collector",
    include_package_data=True,
    zip_safe=False,
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Environment :: Web Environment",
        "Framework :: Flask",
        "Intended Audience :: Developers",
        "Intended Audience :: System Administrators",
        "License :: Other/Proprietary License",
        "Natural Language :: English",
        "Operating System :: MacOS :: MacOS X",
        "Operating System :: POSIX :: Linux",
        "Programming Language :: Python :: 2.7"
        "Topic :: Internet :: WWW/HTTP :: Dynamic Content",
        "Topic :: Internet :: WWW/HTTP :: WSGI",
        "Topic :: Internet :: WWW/HTTP :: WSGI :: Application",
        "Topic :: Software Development",
    ],
)
