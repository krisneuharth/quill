from unittest import TestCase
from quill.analyzer.commas import over_comma_limit


class CommasTestCase(TestCase):

    def test_is_over_comma_limit(self):
        # Over
        self.assertTrue(
            over_comma_limit('This, is, definitely, excessive, literally.', 2)
        )

        # Equal -> Over
        self.assertTrue(
            over_comma_limit('This, is, definitely excessive literally.', 2)
        )

    def test_is_not_over_comma_limit(self):
        self.assertFalse(
            over_comma_limit('This is definitely excessive, literally.', 2)
        )