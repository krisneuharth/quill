from unittest import TestCase
from quill.analyzer.dates import contains_date


class DatesTestCase(TestCase):

    def test_contains_date(self):
        self.assertTrue(
            contains_date('On February 2, 2013 the witness fled the scene.')
        )

    def test_contains_complex_date(self):
        self.assertTrue(
            contains_date("Today is 25 of September of 2003, exactly at 10:49:41 with timezone -03:00.")
        )

    def test_contains_no_date(self):
        self.assertFalse(
            contains_date('In February 2013 the witness fled the scene.')
        )
