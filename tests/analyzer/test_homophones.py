from unittest import TestCase

from quill.analyzer.homophones import HOMOPHONES_DICTIONARY, contains_homophone
from quill.analyzer.homophones import is_homophone


class HomophonesTestCase(TestCase):

    def test_load_dictionary(self):
        self.assertTrue(len(HOMOPHONES_DICTIONARY) > 0)

    def test_is_homophone(self):
        self.assertTrue(is_homophone('trial'))
        self.assertTrue(is_homophone('trail'))

        self.assertFalse(is_homophone('test_word'))

    def test_contains_homophone(self):
        self.assertTrue(
            contains_homophone('The trial will begin soon.')
        )

        self.assertFalse(
            contains_homophone('This example sentence will pass without homophones.')
        )
