from unittest import TestCase
from quill.analyzer.however import contains_however


class HoweverTestCase(TestCase):

    def test_contains_however(self):
        self.assertTrue(
            contains_however('This sentence contains however.')
        )

        self.assertTrue(
            contains_however('This sentence contains HOWEVER.')
        )

        self.assertFalse(
            contains_however('This sentence does not.')
        )
