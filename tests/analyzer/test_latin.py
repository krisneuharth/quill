from unittest import TestCase
from quill.analyzer.latin import LATIN_DICTIONARY
from quill.analyzer.latin import is_latin
from quill.analyzer.latin import contains_latin


class LatinTestCase(TestCase):

    def test_load_dictionary(self):
        self.assertTrue(len(LATIN_DICTIONARY) > 0)

    def test_is_latin(self):
        self.assertTrue(is_latin('veto'))
        self.assertTrue(is_latin('viz.'))

        self.assertFalse(is_latin('test_word'))

    def test_contains_latin(self):
        self.assertTrue(contains_latin('The court will veto the motion.'))
        self.assertTrue(contains_latin('The court will subpoena the witness.'))

        self.assertFalse(contains_latin('This is a test sentence.'))