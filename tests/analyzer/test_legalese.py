from unittest import TestCase
from quill.analyzer.legalese import LEGALESE_DICTIONARY
from quill.analyzer.legalese import is_legalese
from quill.analyzer.legalese import contains_legalese


class LegaleseTestCase(TestCase):

    def test_load_dictionary(self):
        self.assertTrue(len(LEGALESE_DICTIONARY) > 0)

    def test_is_legalese(self):
        self.assertTrue(is_legalese('by means of'))
        self.assertTrue(is_legalese('utilize'))
        self.assertTrue(is_legalese('cause of action'))

        self.assertFalse(is_legalese('test phrase'))

    def test_contains_legalese(self):
        self.assertTrue(contains_legalese('The defendant will prove by means of'))
        self.assertTrue(contains_legalese('Plaintiff will utilize the facts.'))
        self.assertTrue(contains_legalese('Our cause of action is clear.'))

        self.assertFalse(contains_legalese('This is a test sentence.'))
