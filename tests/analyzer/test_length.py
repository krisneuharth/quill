from unittest import TestCase
from quill.analyzer.length import over_paragraph_length, over_sentence_length
from quill.common.models import Document, Sentence, Paragraph


class SentenceLengthTestCase(TestCase):

    def test_over_sentence_length(self):
        self.assertTrue(
            over_sentence_length('This is a longer sentence with many words in it.', 2)
        )

        self.assertTrue(
            over_sentence_length('This is a longer sentence with a different limit.', 9)
        )

        self.assertFalse(
            over_sentence_length('This is sentence.', 9)
        )


class ParagraphLengthTestCase(TestCase):

    def test_over_paragraph_length(self):
        doc = Document(
            sentences=[
                Sentence(pid=1, sid=1),
                Sentence(pid=1, sid=2),
                Sentence(pid=1, sid=3)
            ]
        )

        paragraph = Paragraph(pid=1)

        self.assertTrue(over_paragraph_length(
            doc, paragraph, 2
        ))

    def test_not_over_paragraph_length(self):
        doc = Document(
            sentences=[
                Sentence(pid=1, sid=1),
            ]
        )

        paragraph = Paragraph(pid=1)

        self.assertFalse(over_paragraph_length(
            doc, paragraph, 2
        ))