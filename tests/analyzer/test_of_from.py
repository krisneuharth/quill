from unittest import TestCase
from quill.analyzer.of_from import contains_of_from


class OfAndFromTestCase(TestCase):

    def test_contains_of(self):
        self.assertTrue(
            contains_of_from('This is an example of this.')
        )

        self.assertFalse(
            contains_of_from('No it does not have it.')
        )

    def test_contains_from(self):
        self.assertTrue(
            contains_of_from('This is an example from this.')
        )

        self.assertFalse(
            contains_of_from('No it does not have it.')
        )

    def test_contains_both(self):
        self.assertTrue(
            contains_of_from('This is an example of this from that.')
        )

        self.assertFalse(
            contains_of_from('No it does not have it.')
        )
