from unittest import TestCase

from quill.analyzer.parties import PARTIES_DICTIONARY
from quill.analyzer.parties import is_party
from quill.analyzer.parties import contains_party


class PartiesTestCase(TestCase):

    def test_load_dictionary(self):
        self.assertTrue(len(PARTIES_DICTIONARY) > 0)

    def test_is_party(self):
        self.assertTrue(is_party('Defendant'))
        self.assertTrue(is_party('Plaintiff'))
        self.assertTrue(is_party('Witness'))

        self.assertFalse(is_party('Test Person'))

    def test_contains_party(self):
        self.assertTrue(contains_party('The Defendant will answer the question.'))
        self.assertTrue(contains_party('Plaintiff moves to strike.'))
        self.assertTrue(contains_party('The witness was at the crime scene.'))

        self.assertFalse(contains_party('This is a test sentence.'))
