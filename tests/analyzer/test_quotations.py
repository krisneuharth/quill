from unittest import TestCase
from quill.analyzer.quotations import contains_quotation


class QuotationsTestCase(TestCase):

    def test_contains_quotation(self):
        self.assertTrue(
            contains_quotation('"This is a quotation", he said.')
        )

        self.assertFalse(
            contains_quotation("This is a 'quotation', he said.")
        )
