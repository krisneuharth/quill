from unittest import TestCase

from quill.analyzer.rules import (
    AtomRule, DocumentRule,
    SentenceRule, WordRule,
    ParagraphRule
)


class AtomRulesTestCase(TestCase):

    def test_base_rule(self):
        rule = AtomRule()
        self.assertEqual(rule.name, None)

        with self.assertRaises(NotImplementedError):
            rule.applies_to()
        with self.assertRaises(NotImplementedError):
            rule.test('foo')
        with self.assertRaises(NotImplementedError):
            rule.message()

    def test_document_rule(self):
        rule = DocumentRule()
        self.assertEqual(rule.name, None)
        self.assertEqual(rule.applies_to(), None)

        with self.assertRaises(NotImplementedError):
            rule.test('foo')

        with self.assertRaises(NotImplementedError):
            rule.message()

    def test_paragraph_rule(self):
        rule = ParagraphRule()
        self.assertEqual(rule.name, None)

        with self.assertRaises(NotImplementedError):
            rule.applies_to()

        with self.assertRaises(NotImplementedError):
            rule.test('foo')

    def test_sentence_rule(self):
        rule = SentenceRule()
        self.assertEqual(rule.name, None)

        with self.assertRaises(NotImplementedError):
            rule.applies_to()

        with self.assertRaises(NotImplementedError):
            rule.test('foo')

    def test_word_rule(self):
        rule = WordRule()
        self.assertEqual(rule.name, None)

        with self.assertRaises(NotImplementedError):
            rule.applies_to()

        with self.assertRaises(NotImplementedError):
            rule.test('foo')