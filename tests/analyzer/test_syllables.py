from unittest import TestCase
from quill.analyzer.syllables import _normalize_word
from quill.analyzer.syllables import count_syllables
from quill.analyzer.syllables import is_complex_word


class SyllablesTestCase(TestCase):

    def test_normalize_word(self):
        self.assertEqual(_normalize_word(' Word '), 'word')
        self.assertEqual(_normalize_word(None), None)

    def test_count_syllables(self):
        self.assertEqual(count_syllables('dog'), 1)
        self.assertEqual(count_syllables('doggy'), 2)
        self.assertEqual(count_syllables('telephone'), 3)
        self.assertEqual(count_syllables('dictionary'), 4)

        # Sane error handling
        self.assertEqual(count_syllables(None), 0)

        # Hit the local cache by calling it twice
        self.assertEqual(count_syllables('dog'), 1)

    def test_is_complex_word(self):
        sentence = 'The dictionary contains many words.'
        word = 'dictionary'
        syllables = 4
        self.assertEqual(is_complex_word(sentence, word, syllables), True)

        sentence = 'The doggy is brown.'
        word = 'doggy'
        syllables = 2
        self.assertEqual(is_complex_word(sentence, word, syllables), False)

        sentence = 'Firefighters fight fires.'
        word = 'Firefighters'
        syllables = 4
        self.assertEqual(is_complex_word(sentence, word, syllables), True)

        sentence = 'The Firefighters fight fires.'
        word = 'Firefighters'
        syllables = 4
        self.assertEqual(is_complex_word(sentence, word, syllables), False)