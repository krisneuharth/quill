from unittest import TestCase
from quill.analyzer.this_that import contains_this_that


class ThisAndThatTestCase(TestCase):
    """
    This and That test cases
    """

    def test_has_this(self):
        self.assertTrue(
            contains_this_that('This is an example of this.')
        )

        self.assertFalse(
            contains_this_that('No it does not have it.')
        )

    def test_has_that(self):
        self.assertTrue(
            contains_this_that('That is an example.')
        )

        self.assertFalse(
            contains_this_that('No it does not have it.')
        )

    def test_has_both(self):
        self.assertTrue(
            contains_this_that('This is an example of this and that.')
        )

        self.assertFalse(
            contains_this_that('No it does not have it.')
        )
